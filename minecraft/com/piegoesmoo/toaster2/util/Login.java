package com.piegoesmoo.toaster2.util;

import com.mojang.authlib.Agent;
import com.mojang.authlib.UserAuthentication;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;

import net.minecraft.client.Minecraft;
import net.minecraft.util.Session;

import java.net.Proxy;
import java.util.UUID;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/27/2014
 */
public class Login implements Runnable {

	private final UserAuthentication ua;
	private String username;

	public Login(String username, String password) {
		YggdrasilAuthenticationService service = new YggdrasilAuthenticationService(Proxy.NO_PROXY, UUID.randomUUID().toString());
		this.ua = service.createUserAuthentication(Agent.MINECRAFT);
		this.ua.setUsername(this.username = username);
		this.ua.setPassword(password);
	}

	@Override
	public void run() {
		try {
			ua.logIn();
			Minecraft.getMinecraft().setSession(new Session(ua.getSelectedProfile().getName(), ua.getSelectedProfile().getId().toString(), ua.getAuthenticatedToken(), this.username.contains("@") ? "mojang" : "legacy"));
		} catch (AuthenticationException e) {
			e.printStackTrace();
		}
	}

	public static Session authenticate(String username, String password) {
		YggdrasilAuthenticationService service = new YggdrasilAuthenticationService(Proxy.NO_PROXY, UUID.randomUUID().toString());
		UserAuthentication ua;
		ua = service.createUserAuthentication(Agent.MINECRAFT);
		ua.setUsername(username);
		ua.setPassword(password);
		try {
			ua.logIn();
			return new Session(ua.getSelectedProfile().getName(), ua.getSelectedProfile().getId().toString(), ua.getAuthenticatedToken(), username.contains("@") ? "mojang" : "legacy");
		} catch (AuthenticationException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}