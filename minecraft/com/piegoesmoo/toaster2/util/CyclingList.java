package com.piegoesmoo.toaster2.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/1/2014
 */
public class CyclingList<T> extends ArrayList<T> {
	private int curIndex = -1;

	public CyclingList() {
	}

	public CyclingList(List<T> list) {
		for (T t : list)
			this.add(t);
	}

	public T next() {
		curIndex++;
		if (curIndex > this.size() - 1)
			curIndex = 0;
		return this.get(curIndex);
	}
}
