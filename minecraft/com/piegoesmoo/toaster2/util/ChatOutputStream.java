package com.piegoesmoo.toaster2.util;

import com.piegoesmoo.toaster2.Toaster;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/31/2014
 */
public class ChatOutputStream extends PrintStream {
	private static final MinecraftChatOutput CHAT_LOGGER = Toaster.CHAT_OUTPUT;

	public ChatOutputStream(OutputStream out) {
		super(out);
	}

	@Override public void println(String x) {
		CHAT_LOGGER.log(x + "\n");
		super.println();
	}

	@Override public void println() {
		println("");
	}

	@Override public void println(long x) {
		println(String.valueOf(x));
	}

	@Override public void println(float x) {
		println(String.valueOf(x));
	}

	@Override public void println(double x) {
		println(String.valueOf(x));
	}

	@Override public void println(char[] x) {
		println(String.valueOf(x));
	}

	@Override public void println(boolean x) {
		println(String.valueOf(x));
	}

	@Override public void print(boolean b) {
		CHAT_LOGGER.log(String.valueOf(b));
	}

	@Override public void println(char x) {
		println(String.valueOf(x));
	}

	@Override public void println(int x) {
		println(String.valueOf(x));
	}
}
