package com.piegoesmoo.toaster2.util;

import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.property.Property;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.File;
import java.io.FileWriter;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/14/2014
 */
public class XMLUtils {
	public static final File CONFIG_FILE = new File(Toaster.DATA_DIR, "config.xml");

	private XMLUtils() {
	}

	public static void createXML() {
		try {
			Element configElement = new Element("config");
			Document document = new Document(configElement);

			Element propertyElement;
			for (Plugin p : Toaster.getToaster().pluginManager.getPluginList()) {
				Element pluginElement = new Element(p.safeName);
				for (Property property : p.getPropertyList()) {
					pluginElement.addContent(propertyElement = new Element("property"));
					propertyElement.setAttribute(property.getName().replaceAll(p.safeName + ".", ""), property.stringValue());
				}
				document.getRootElement().addContent(pluginElement);
			}

			Element globalElement = new Element("global");
			for (Property property : Toaster.getToaster().globalProps) {
				globalElement.addContent(new Element(property.getName()).setText(property.stringValue()));
			}
			document.getRootElement().addContent(globalElement);

			XMLOutputter xmlOutputter = new XMLOutputter();
			xmlOutputter.setFormat(Format.getPrettyFormat());
			xmlOutputter.output(document, new FileWriter(CONFIG_FILE));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void readXML() {
		if (CONFIG_FILE.exists()) {
			try {
				SAXBuilder builder = new SAXBuilder();

				Document document = builder.build(CONFIG_FILE);
				Element rootElement = document.getRootElement();

				for (Element element : rootElement.getChildren()) {
					if (element.getName().equals("global")) {//load global properties
						for (Property property : Toaster.getToaster().globalProps)
							property.fromString(element.getChildText(property.getName()));
						return;
					}

					Plugin plugin = Toaster.getToaster().pluginManager.getPlugin(element.getName());
					if (plugin != null) {
						for (Element element1 : element.getChildren()) {
							for (Property property : plugin.getPropertyList()) {
								String val = element1.getAttributeValue(property.getName().replaceAll(plugin.safeName + ".", ""));
								if (val != null && !val.isEmpty() && val.length() > 0) {
									property.fromString(val);
								}
							}
						}
						plugin.load();
						Toaster.getToaster().keybindManager.addKeybindable(plugin);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
