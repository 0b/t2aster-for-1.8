package com.piegoesmoo.toaster2.util;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/5/2014
 */
public enum EnumEventType {
	PRE,
	POST
}
