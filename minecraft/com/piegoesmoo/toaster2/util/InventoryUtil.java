package com.piegoesmoo.toaster2.util;

import net.minecraft.client.Minecraft;
import net.minecraft.inventory.Slot;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/24/2014
 */
public class InventoryUtil {

	public static void clickSlot(Slot slot, MouseButton button, boolean shift) {
		Minecraft.getMinecraft().playerController.windowClick(Minecraft.getMinecraft().thePlayer.inventoryContainer.windowId,
				slot.slotNumber, button.ordinal(), shift ? 1 : 0, Minecraft.getMinecraft().thePlayer);
	}

	public static void clickSlot(int slot, MouseButton button, boolean shift) {
		Minecraft.getMinecraft().playerController.windowClick(Minecraft.getMinecraft().thePlayer.inventoryContainer.windowId,
				slot, button.ordinal(), shift ? 1 : 0, Minecraft.getMinecraft().thePlayer);
	}

	public enum MouseButton {
		LEFT,
		RIGHT
	}
}
