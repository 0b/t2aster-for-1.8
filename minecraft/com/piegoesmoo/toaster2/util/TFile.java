package com.piegoesmoo.toaster2.util;

import com.piegoesmoo.toaster2.Toaster;

import java.io.File;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/5/2014
 */
public class TFile extends File {
	public TFile(String child) {
		super(Toaster.DATA_DIR, child + (child.endsWith(".xml") ? "" : ".bin"));
	}
}
