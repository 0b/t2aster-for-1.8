package com.piegoesmoo.toaster2.util;

import com.piegoesmoo.toaster2.Toaster;
import net.minecraft.util.BlockPos;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/5/2014
 */
public class PlayerUtil {
	private PlayerUtil() {
	}

	public static double getDistanceToBlockPos(BlockPos blockPos) {
		return Toaster.mc.thePlayer.getDistance(blockPos.getX(), blockPos.getY(), blockPos.getZ());
	}
}
