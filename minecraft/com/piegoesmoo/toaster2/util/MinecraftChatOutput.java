package com.piegoesmoo.toaster2.util;

import com.piegoesmoo.toaster2.Toaster;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ChatComponentText;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/28/2014
 */
public class MinecraftChatOutput {

	public void log(String s) {
		Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("\2479[" + Toaster.clientName.stringValue() + "]: " + "\2477" + s));
	}

	public void logf(String s, Object... args) {
		try {
			log(String.format(s, args));
		} catch (Exception e) {
			System.out.println("You forgot arguments, dumbass.");
			e.printStackTrace();
		}
	}

	public void error(String s) {
		Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("\2479[" + Toaster.clientName.stringValue() + "]: " + "\247c" + s));
	}

	public void errorf(String s, Object... args) {
		try {
			error(String.format(s, args));
		} catch (Exception e) {
			System.out.println("You forgot arguments, dumbass.");
			e.printStackTrace();
		}
	}
}
