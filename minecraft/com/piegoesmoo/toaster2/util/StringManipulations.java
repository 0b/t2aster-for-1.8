package com.piegoesmoo.toaster2.util;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/24/2014
 */
public class StringManipulations {
	private StringManipulations() {
	}

	public static String escapeToUnicode(String s) {
		String out = "";
		for (char c : s.toCharArray()) {
			out += "\\u00" + Integer.toHexString(c);
		}
		return out;
	}
}
