package com.piegoesmoo.toaster2.util;

import java.util.Arrays;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/1/2014
 */
public enum Protocol {
	P1_7_10(5, "1.7.10"),
	P1_8(47, "1.8");

	private final int protocolVersion;
	private final String versionName;

	Protocol(final int version, String versionName) {
		this.protocolVersion = version;
		this.versionName = versionName;
	}

	public int getProtocolVersion() {
		return protocolVersion;
	}

	public String getVersionName() {
		return versionName;
	}

	public static final CyclingList<Protocol> protocols = new CyclingList<>(Arrays.asList(values()));
}
