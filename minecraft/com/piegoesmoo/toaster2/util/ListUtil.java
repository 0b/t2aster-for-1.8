package com.piegoesmoo.toaster2.util;

import java.util.List;
import java.util.Random;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/23/2014
 */
public class ListUtil {
	private static final Random RANDOM = new Random();

	public static <T> T selectRandomElement(List<T> list) {
		return list.get(RANDOM.nextInt(list.size()));
	}

	public static <T> T selectRandomElement(T[] list) {
		return list[(RANDOM.nextInt(list.length))];
	}
}
