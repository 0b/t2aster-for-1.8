package com.piegoesmoo.toaster2.util;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/27/2014
 */
public class Timer {
	private long last = -1L;

	public boolean timePassed(long l) {
		boolean b = last == -1L || System.currentTimeMillis() >= l + last;
		if (b)
			reset();
		return b;
	}

	/**
	 * No longer needed for manual use. Automatically resets the time.
	 */
	private void reset() {
		last = System.currentTimeMillis();
	}
}
