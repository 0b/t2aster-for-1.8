package com.piegoesmoo.toaster2.util;

import java.awt.*;

import static org.lwjgl.opengl.GL11.*;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/11/2014
 */
public class GLUtil {
	private GLUtil() {
	}

	/*
	 *	thanks dankstorm hahahaha fag
	 */
	public static void setColor(Color c) {
		glColor4f(c.getRed() / 255f, c.getGreen() / 255f, c.getBlue() / 255f, 1F);
	}

	/*
	 *	thanks dankstorm hahahaha fag
	 */
	public static void setColor(int rgba) {
		int r = rgba & 0xFF, g = rgba >> 8 & 0xFF, b = rgba >> 16 & 0xFF, a = rgba >> 24 & 0xFF;
		glColor4b((byte) r, (byte) g, (byte) b, (byte) a);
	}

	public static void radialGrad(float xPos, float yPos, int innerColor, int outerColor, int slices, float radius) {
		float incr = (float) (2 * Math.PI / slices);
		glBegin(GL_TRIANGLE_FAN);
		setColor(innerColor);
		glVertex2f(xPos, yPos);
		setColor(outerColor);
		for (int i = 0; i < slices; i++) {
			float angle = incr * i;
			float x = (float) Math.cos(angle) * radius;
			float y = (float) Math.sin(angle) * radius;
			glVertex2f(x, y);
		}
		glVertex2f(radius, 0.0f);

		glEnd();
	}
}
