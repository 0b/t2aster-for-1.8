package com.piegoesmoo.toaster2.util;

import net.minecraft.entity.Entity;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/5/2014
 */
public class Location {
	private int x, y, z;

	public Location(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Location(Entity entity) {
		this((int) entity.posX, (int) entity.posY, (int) entity.posZ);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}
}
