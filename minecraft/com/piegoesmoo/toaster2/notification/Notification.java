package com.piegoesmoo.toaster2.notification;

import com.piegoesmoo.toaster2.util.Timer;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/2/2014
 */
public class Notification {
	private final String title, message, subMessage;
	private final Timer timer = new Timer();

	public Notification(String title, String message, String subMessage) {
		this.title = title;
		this.message = message;
		this.subMessage = subMessage;
	}

	public String getTitle() {
		return title;
	}

	public String getMessage() {
		return message;
	}

	public String getSubMessage() {
		return subMessage;
	}
}
