package com.piegoesmoo.toaster2.notification;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventIngameRender;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.Gui;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;

import java.util.HashMap;
import java.util.Map;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/2/2014
 */
public class NotificationManager {
	private static final Minecraft mc = Minecraft.getMinecraft();
	private final Map<Long, Notification> notifications = new HashMap<>();

	public NotificationManager() {
		EventManager.register(this);
	}

	public void sendNotification(Notification n) {
		notifications.put(System.currentTimeMillis(), n);
		mc.getSoundHandler().playSound(PositionedSoundRecord.createPositionedSoundRecord(new ResourceLocation("random.pop"), 0.5F));
	}

	@EventTarget public void draw(EventIngameRender eventIngameRender) {
		int yPos = eventIngameRender.getScaledResolution().getScaledHeight();
		for (Map.Entry<Long, Notification> entry : notifications.entrySet()) {
			Notification n = entry.getValue();

			if (entry.getKey() + 3000 <= System.currentTimeMillis()) {
				notifications.remove(entry.getKey());
			}

			Gui.drawRect(eventIngameRender.getScaledResolution().getScaledWidth() - 150, yPos -= 30, eventIngameRender.getScaledResolution().getScaledWidth() - 1, yPos + 29, 0xFF222222);
			mc.fontRendererObj.drawStringWithShadow(EnumChatFormatting.AQUA + n.getTitle(), eventIngameRender.getScaledResolution().getScaledWidth() - 147, yPos + 2, 0xFFFFFF);
			mc.fontRendererObj.drawStringWithShadow(n.getMessage(), eventIngameRender.getScaledResolution().getScaledWidth() - 147, yPos + 11, 0xFFFFFF);
			mc.fontRendererObj.drawStringWithShadow(n.getSubMessage(), eventIngameRender.getScaledResolution().getScaledWidth() - 147, yPos + 20, 0xFFFFFF);
		}
	}
}
