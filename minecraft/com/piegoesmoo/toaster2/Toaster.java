/*
 * Copyright (c) 2014 piegoesmoo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package com.piegoesmoo.toaster2;

import com.piegoesmoo.toaster2.cmd.CommandManager;
import com.piegoesmoo.toaster2.hook.TGuiIngame;
import com.piegoesmoo.toaster2.irc.IRCHandler;
import com.piegoesmoo.toaster2.keybind.KeybindManager;
import com.piegoesmoo.toaster2.macro.MacroManager;
import com.piegoesmoo.toaster2.notification.NotificationManager;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginManager;
import com.piegoesmoo.toaster2.profile.ProfileManager;
import com.piegoesmoo.toaster2.property.Property;
import com.piegoesmoo.toaster2.property.properties.StringProperty;
import com.piegoesmoo.toaster2.util.MinecraftChatOutput;
import com.piegoesmoo.toaster2.util.Protocol;
import com.piegoesmoo.toaster2.util.XMLUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.Display;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

// TODO: Make IRC not suck ass or get rid of it. TBD
// FIXME: AutoArmor
// FIXME: Protocol or get rid of it. TBD

public class Toaster {

	private static Toaster __instance;

	public static StringProperty clientName = new StringProperty("client.name", "t2aster") {
		@Override public void onValueSet(String value) {
			Display.setTitle(value);
		}
	};

	public static final List<String> DONORS = new ArrayList<>();
	public static final ResourceLocation CAPE_LOCATION = new ResourceLocation("toaster/t2aster_cape.png");
	public static final String VER = "version 1.0";
	public static final File DATA_DIR = new File(Minecraft.getMinecraft().mcDataDir, "t2aster");
	public static final MinecraftChatOutput CHAT_OUTPUT = new MinecraftChatOutput();
	public static final Minecraft mc = Minecraft.getMinecraft();
	public static final int FRIEND_COLOR = 0xFFb5d02a;
	public static final int GUI_COLOR = 0xFF4f4fff;

	public final PluginManager pluginManager = new PluginManager();
	public final KeybindManager keybindManager = new KeybindManager();
	public final CommandManager commandManager = new CommandManager();
	public final ProfileManager profileManager = new ProfileManager();
	public final MacroManager macroManager = new MacroManager();
	public IRCHandler ircHandler;
	public final NotificationManager notificationManager = new NotificationManager();

	public final List<Property> globalProps = new ArrayList<>();

	public Protocol currentProtocol = Protocol.P1_8;


	public Toaster() {
		if (!DATA_DIR.exists())
			DATA_DIR.mkdir();

		globalProps.add(clientName);

		Runtime.getRuntime().addShutdownHook(new Thread("Toaster Shutdown Thread") {
			@Override
			public void run() {
				for (Plugin plugin : pluginManager.getPluginList()) {
					plugin.unload();
				}
				profileManager.saveProfiles();
				macroManager.saveMacros();
				XMLUtils.createXML();
			}
		});

		try {
			URL donorUrl = new URL("http://piegoesmoo.com/donors.txt");
			BufferedReader in = new BufferedReader(new InputStreamReader(donorUrl.openStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				DONORS.add(inputLine);
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void init() {
		pluginManager.init();
		XMLUtils.readXML();
		commandManager.init();
		profileManager.init();
		keybindManager.init();
		macroManager.init();

		Display.setTitle(clientName.stringValue());
		Minecraft.getMinecraft().ingameGUI = new TGuiIngame(mc);
	}

	public static Toaster getToaster() {
		if (__instance == null) {
			__instance = new Toaster();
		}
		return __instance;
	}
}
