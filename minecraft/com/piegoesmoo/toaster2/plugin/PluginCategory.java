package com.piegoesmoo.toaster2.plugin;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/14/2014
 */
public enum PluginCategory {
	COMBAT(0xFFE21418),
	MOVEMENT(0xFF13C422),
	WORLD(0xFF6495ED),
	RENDER(0xFFD9D9D9),
	PLAYER(0xFF3FD987),
	OTHER(-1);

	private final int color;

	private PluginCategory(int color) {
		this.color = color;
	}

	public int getColor() {
		return color;
	}

	@Override public String toString() {
		return super.toString().substring(0, 1) + super.toString().substring(1).toLowerCase();
	}
}
