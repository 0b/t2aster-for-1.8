package com.piegoesmoo.toaster2.plugin;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/25/2014
 */
public abstract class ExternalPlugin extends Plugin {
	public ExternalPlugin(String name, PluginCategory category) {
		super(name, category);
	}

	public ExternalPlugin(String name, PluginCategory category, int key) {
		super(name, category, key);
	}
}
