package com.piegoesmoo.toaster2.plugin;

import com.darkmagician6.eventapi.EventManager;
import com.piegoesmoo.toaster2.keybind.Bindable;
import com.piegoesmoo.toaster2.property.Property;
import com.piegoesmoo.toaster2.property.properties.BoolProperty;
import com.piegoesmoo.toaster2.property.properties.NumberProperty;
import com.piegoesmoo.toaster2.property.properties.StringProperty;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Keyboard;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/20/2014
 */
public class Plugin extends Bindable {
	protected static final Minecraft mc = Minecraft.getMinecraft();

	private static final Random RANDOM = new Random();
	private final List<Property> propertyList = new ArrayList<>();

	private final StringProperty nameProperty;
	private final BoolProperty stateProperty;
	private final NumberProperty keybindProperty;

	private final PluginCategory category;

	public final String safeName;

	public final int color = randCol();

	public Plugin(String name) {
		this(name, PluginCategory.OTHER, Keyboard.KEY_NONE);
	}

	public Plugin(String name, PluginCategory category) {
		this(name, category, Keyboard.KEY_NONE);
	}

	public Plugin(String name, PluginCategory category, int keybind) {
		safeName = name.replaceAll(" ", "").toLowerCase();
		this.addProperty(nameProperty = new StringProperty(safeName + ".name", name),
				stateProperty = new BoolProperty("state", false),
				keybindProperty = new NumberProperty("keybind", keybind));
		this.category = category;
	}

	public void load() {
		try {
			for (Field field : this.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				Object o = field.get(this);
				if (o != null && Property.class.isAssignableFrom(o.getClass())) {
					this.addProperty((Property) o);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (stateProperty.getValue())
			onEnable();
	}

	public void unload() {
	}

	public void onEnable() {
		EventManager.register(this);
	}

	public void onDisable() {
		EventManager.unregister(this);
	}

	public void toggle() {
		stateProperty.setValue(!stateProperty.getValue());
		if (stateProperty.getValue()) onEnable();
		else onDisable();
	}

	public boolean getState() {
		return stateProperty.getValue();
	}

	private int randCol() {
		return RANDOM.nextInt(255) | RANDOM.nextInt(255) << 8 | RANDOM.nextInt(255) << 16 | 255 << 24;
	}

	public void addProperty(Property... property) {
		Collections.addAll(propertyList, property);
	}

	public List<Property> getPropertyList() {
		return propertyList;
	}

	public Property<?> findProperty(String s) {
		for (Property property : propertyList)
			if (property.getName().equalsIgnoreCase(s))
				return property;
		return null;
	}

	public String getName() {
		return nameProperty.stringValue();
	}

	public void setName(String name) {
		nameProperty.setValue(name);
	}

	@Override public int getKeybind() {
		return keybindProperty.getValue().intValue();
	}

	@Override public void setKeybind(int keybind) {
		keybindProperty.setValue(keybind);
	}

	@Override public void fire() {
		toggle();
	}

	public PluginCategory getCategory() {
		return category;
	}
}
