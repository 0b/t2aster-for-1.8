package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventTick;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/26/2014
 */
public class AutoRespawn extends Plugin {
	public AutoRespawn() {
		super("Auto Respawn", PluginCategory.COMBAT);
	}

	@EventTarget public void tick(EventTick eventTick) {
		if (mc.thePlayer.isDead)
			mc.thePlayer.respawnPlayer();
	}
}
