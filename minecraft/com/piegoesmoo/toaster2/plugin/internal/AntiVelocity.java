package com.piegoesmoo.toaster2.plugin.internal;

import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/22/2014
 */
public class AntiVelocity extends Plugin {
	public AntiVelocity() {
		super("Anti Velocity", PluginCategory.COMBAT);
	}
}
