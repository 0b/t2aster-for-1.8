package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.events.EventPacket;
import com.piegoesmoo.toaster2.events.EventUpdate;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.property.properties.NumberProperty;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.C03PacketPlayer;
import org.lwjgl.opengl.Display;

public class Step extends Plugin {
	public NumberProperty height = new NumberProperty("step.height", 1);
	private boolean packetFixTriggered;

	public Step() {
		super("Step", PluginCategory.MOVEMENT);
		this.addProperty(height);
	}

	@EventTarget private void onUpdate(EventUpdate e) {
		mc.thePlayer.stepHeight = height.getValue().floatValue();
		Display.setTitle(Toaster.clientName.stringValue());
	}

	@Override public void onDisable() {
		mc.thePlayer.stepHeight = 0.5F;
		super.onDisable();
	}

	public void triggerPacketFix() {
		packetFixTriggered = true;
	}

	public void onPacket(EventPacket e) {
		Packet packet = e.getPacket();
		if (e.type.equals(EventPacket.PacketType.SEND) && packetFixTriggered && packet instanceof C03PacketPlayer.C04PacketPlayerPosition) {
			C03PacketPlayer.C04PacketPlayerPosition c03 = (C03PacketPlayer.C04PacketPlayerPosition) packet;
			c03.y += 0.1;
			packetFixTriggered = false;
		}
	}

}
