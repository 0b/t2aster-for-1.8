package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.events.EventUpdate;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.property.properties.BoolProperty;
import com.piegoesmoo.toaster2.property.properties.NumberProperty;
import com.piegoesmoo.toaster2.util.EnumEventType;
import com.piegoesmoo.toaster2.util.Timer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemSword;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/27/2014
 */
public class KillAura extends Plugin {
	private static final Random RANDOM = new Random();

	public NumberProperty speed = new NumberProperty("killaura.speed", 14);
	public NumberProperty range = new NumberProperty("killaura.range", 4);

	private BoolProperty animal = new BoolProperty("killaura.animal", false);
	private BoolProperty mob = new BoolProperty("killaura.mob", false);
	private BoolProperty player = new BoolProperty("killaura.player", true);
	private BoolProperty friend = new BoolProperty("killaura.friend", false);
	private BoolProperty block = new BoolProperty("killaura.block", true);

	private int testInt;
	private final Timer timer = new Timer();

	public KillAura() {
		super("Kill Aura", PluginCategory.COMBAT, Keyboard.KEY_F);
	}

	@EventTarget private void onPreUpdate(EventUpdate e) {
		List<EntityLivingBase> l = getSortedList(range.getValue().doubleValue());
		if (testInt > l.size() - 1) {
			testInt = 0;
		}
		EntityLivingBase el = l.get(testInt);
		boolean b = timer.timePassed((1000 / speed.getValue().intValue()) - RANDOM.nextInt(20));
		boolean wasSprint = mc.thePlayer.isSprinting();
		if (e.type.equals(EnumEventType.PRE)) {
			if (el != null && isEntityEligible(el)) {
				mc.thePlayer.setSprinting(false);
				if (b) {
					mc.thePlayer.swingItem();
					mc.playerController.attackEntity(mc.thePlayer, el);
					testInt++;
				}
			}
		} else {
			mc.thePlayer.setSprinting(wasSprint);
			if (block.getValue() && mc.currentScreen == null && el != null && System.currentTimeMillis() % 3 == 0)
				if (mc.thePlayer.inventory.getCurrentItem() != null && mc.thePlayer.inventory.getCurrentItem().getItem() instanceof ItemSword)
					mc.playerController.sendUseItem(mc.thePlayer, mc.theWorld, mc.thePlayer.inventory.getCurrentItem());
		}
	}

	public List<EntityLivingBase> getSortedList(double range) {
		List<EntityLivingBase> list = new ArrayList<>();
		for (Entity e : mc.theWorld.loadedEntityList) {
			if (e instanceof EntityLivingBase) {
				EntityLivingBase livingBase = (EntityLivingBase) e;
				if (!isEntityEligible(livingBase))
					continue;

				list.add(livingBase);
			}
		}
		if (list.isEmpty()) {
			return list;
		}
		List<EntityLivingBase> sorted = new ArrayList<>();
		EntityLivingBase next;
		while ((next = this.getHighestThreat(list, range)) != null) {
			list.remove(next);
			sorted.add(next);
		}
		return sorted;
	}

	public EntityLivingBase getHighestThreat(List<EntityLivingBase> threatList, double range) {
		double highestThreat = isPlayerMoving() ? range - 0.3 : range;
		EntityLivingBase bigThreat = null;
		for (EntityLivingBase e : threatList) {
			if (Toaster.getToaster().profileManager.isFriend(e.getName()) && !friend.getValue())
				continue;
			double threat = mc.thePlayer.getDistanceToEntity(e);
			if (threat < highestThreat) {
				highestThreat = threat;
				bigThreat = e;
			}
		}

		return bigThreat;
	}

	private boolean canAttackEntity(EntityLivingBase entity) {
		return !entity.equals(mc.thePlayer)
				&& mc.thePlayer.canEntityBeSeen(entity)
				&& mc.thePlayer.getDistanceToEntity(entity) <= range.getValue().doubleValue()
				&& entity.deathTime <= 0
				&& entity.isEntityAlive();
	}

	private boolean isEntityEligible(EntityLivingBase entity) {
		return canAttackEntity(entity) &&
				((entity instanceof EntityAnimal && animal.getValue())
						|| (entity instanceof EntityMob && mob.getValue())
						|| (entity instanceof EntityPlayer && player.getValue()));
	}

	private boolean isPlayerMoving() {
		return mc.thePlayer.moveForward > 0 || mc.thePlayer.moveStrafing > 0;
	}

	private void block() {
		if (mc.thePlayer.inventory.getCurrentItem().getItem() instanceof ItemSword) {
			mc.thePlayer.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement());
		}
	}
}
