package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventUpdate;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.util.EnumEventType;
import net.minecraft.client.entity.EntityOtherPlayerMP;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/5/2014
 */
public class Freecam extends Plugin {
	private EntityOtherPlayerMP freecamPlayer;

	public Freecam() {
		super("Freecam", PluginCategory.PLAYER);
	}

	@Override public void onEnable() {
		super.onEnable();
		freecamPlayer = new EntityOtherPlayerMP(mc.theWorld, mc.thePlayer.getGameProfile());
		freecamPlayer.copyLocationAndAnglesFrom(mc.thePlayer);
		freecamPlayer.inventory = mc.thePlayer.inventory;
		mc.theWorld.addEntityToWorld(-666, freecamPlayer);
	}

	@EventTarget public void tick(EventUpdate eventUpdate) {
		if (eventUpdate.type.equals(EnumEventType.PRE))
			eventUpdate.setCancelled(true);
	}

	@Override public void onDisable() {
		mc.thePlayer.copyLocationAndAnglesFrom(freecamPlayer);
		mc.theWorld.removeEntityFromWorld(-666);
		freecamPlayer = null;
		mc.thePlayer.noClip = false;
		super.onDisable();
	}
}
