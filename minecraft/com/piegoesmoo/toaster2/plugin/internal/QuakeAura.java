package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventUpdate;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.util.EnumEventType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemHoe;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import org.lwjgl.input.Keyboard;


public class QuakeAura extends Plugin {

	private float fakeYaw, fakePitch;

	public QuakeAura() {
		super("Quack Aura", PluginCategory.COMBAT, Keyboard.KEY_L);
	}

	@EventTarget public void update(EventUpdate eventUpdate) {
		if (eventUpdate.type.equals(EnumEventType.PRE)) {
			this.fakePitch = mc.thePlayer.rotationPitch;
			this.fakeYaw = mc.thePlayer.rotationYaw;
			mc.theWorld.playerEntities.stream().filter(this::canAim).forEach(entity -> silentAim(entity));
			mc.theWorld.playerEntities.stream().filter(this::canGib).forEach(ep -> mc.thePlayer.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(mc.thePlayer.inventory.getCurrentItem())));
		} else {
			mc.thePlayer.rotationPitch = this.fakePitch;
			mc.thePlayer.rotationYaw = this.fakeYaw;
		}
	}

	private void silentAim(EntityPlayer entity) {
		boolean sprinting = entity.isSprinting();
		double xRelapse = entity.posX - entity.lastTickPosX;
		double zRelapse = entity.posZ - entity.lastTickPosZ;
		double xLead = 1.0D;
		double zLead = 1.0D;
		double d = mc.thePlayer.getDistanceToEntity(entity);
		double x = entity.posX + xLead - mc.thePlayer.posX;
		double z = entity.posZ + zLead - mc.thePlayer.posZ;
		double y = mc.thePlayer.posY + mc.thePlayer.getEyeHeight() - (
				entity.posY + entity.getEyeHeight());
		double dist = mc.thePlayer.getDistanceToEntity(entity);

		d -= d % 2.0D;
		xLead = d / 2.0D * xRelapse * (sprinting ? 1.25D : 1.0D);
		zLead = d / 2.0D * zRelapse * (sprinting ? 1.25D : 1.0D);
		mc.thePlayer.rotationYaw = (float) Math.toDegrees(Math.atan2(z, x)) - 90.0F;
		mc.thePlayer.rotationPitch = (float) Math.toDegrees(Math.atan2(y, dist));
	}

	public boolean canAim(EntityPlayer ep) {
		return !(ep.equals(mc.thePlayer))
				&& mc.thePlayer.canEntityBeSeen(ep)
				&& mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemHoe;
	}

	public boolean canGib(EntityPlayer ep) {
		return !(ep.equals(mc.thePlayer))
				&& mc.thePlayer.canEntityBeSeen(ep)
				&& mc.thePlayer.experience == 0
				&& mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemHoe;
	}


}
