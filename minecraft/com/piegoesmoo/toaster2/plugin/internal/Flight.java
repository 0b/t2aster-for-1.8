package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventTick;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.property.properties.NumberProperty;
import net.minecraft.util.MathHelper;
import org.lwjgl.input.Keyboard;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/27/2014
 */
public class Flight extends Plugin {
	private NumberProperty flySpeed = new NumberProperty("speed", 20);

	public Flight() {
		super("Flight", PluginCategory.MOVEMENT, Keyboard.KEY_R);
	}

	@EventTarget
	public void doFly(EventTick eventTick) {
		mc.thePlayer.motionX = mc.thePlayer.motionY = mc.thePlayer.motionZ = 0;
		if (mc.gameSettings.keyBindJump.getIsKeyPressed())
			mc.thePlayer.motionY += flySpeed.getValue().intValue() / 20;
		if (mc.gameSettings.keyBindSneak.getIsKeyPressed())
			mc.thePlayer.motionY -= flySpeed.getValue().intValue() / 20;
		double forward = mc.thePlayer.moveForward * (flySpeed.getValue().intValue() / 20);
		double strafe = mc.thePlayer.moveStrafing * (flySpeed.getValue().intValue() / 20);

		float var5 = MathHelper.sin(mc.thePlayer.rotationYaw * (float) Math.PI / 180.0F);
		float var6 = MathHelper.cos(mc.thePlayer.rotationYaw * (float) Math.PI / 180.0F);
		mc.thePlayer.motionX += (strafe * var6 - forward * var5);
		mc.thePlayer.motionZ += (forward * var6 + strafe * var5);

		mc.thePlayer.distanceWalkedModified = 0;
	}
}
