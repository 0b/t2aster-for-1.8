package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.Event3DRender;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.item.ItemBow;

import static org.lwjgl.opengl.GL11.*;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/25/2014
 */
public class ProjectileESP extends Plugin {
	public ProjectileESP() {
		super("Projectile ESP", PluginCategory.COMBAT);
	}

	@EventTarget public void onRender(Event3DRender event) {
		if (mc.thePlayer.inventory.getCurrentItem().getItem() instanceof ItemBow) {
			ItemBow bow = (ItemBow) mc.thePlayer.inventory.getCurrentItem().getItem();
			int var6 = bow.getMaxItemUseDuration(mc.thePlayer.getCurrentEquippedItem()) - mc.thePlayer.getItemInUseCount();
			float var7 = (float) var6 / 20.0F;
			var7 = (var7 * var7 + var7 * 2.0F) / 3.0F;
			EntityArrow arrow = new EntityArrow(mc.theWorld, mc.thePlayer, var7 * 2.0F);

			if (!mc.theWorld.getEntitiesWithinAABB(EntityPlayer.class, arrow.getBoundingBox()).isEmpty()) {
				glColor4f(1, 0, 0, 1);
			} else {
				glColor4f(0, 1, 0, 1);
			}

			glBegin(GL_LINES);
			{
				glVertex3d(arrow.posX - 5, arrow.posY, arrow.posZ);
				glVertex3d(arrow.posX + 5, arrow.posY, arrow.posZ);
			}
			glEnd();

			glBegin(GL_LINES);
			{
				glVertex3d(arrow.posX, arrow.posY, arrow.posZ - 5);
				glVertex3d(arrow.posX, arrow.posY, arrow.posZ + 5);
			}
			glEnd();
		}
	}
}
