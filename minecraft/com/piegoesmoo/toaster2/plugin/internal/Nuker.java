package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventBlockClick;
import com.piegoesmoo.toaster2.events.EventUpdate;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.util.EnumEventType;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.util.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Project: Client
 *
 * @author Jayden
 */
public class Nuker extends Plugin {

	private float pitch;
	private float yaw;
	private float serverpitch;
	private float serveryaw;
	private float curBlockDamageMP = 0.0F;
	private double difD = 0.0D;
	private static long currentms;
	private static long last = 0L;
	private static NukerObj nuker = null;
	private ArrayList<NukerObj> nukerList = new ArrayList();
	Random rand = new Random();
	String i = String.valueOf(this.rand.nextInt(1000));
	private boolean click;
	private boolean delay = false;
	private int blockdelay = 0;

	public Nuker() {
		super("Nuker", PluginCategory.WORLD);
	}

	@Override
	public void onDisable() {
		super.onDisable();
		this.nukerList.clear();
		nuker = null;
		this.click = false;
		this.delay = false;
		this.pitch = mc.thePlayer.rotationPitch;
		this.yaw = mc.thePlayer.rotationYaw;
	}

	@EventTarget public void click(EventBlockClick blockClick) {
		if (mc.playerController.isInCreativeMode()) {
			int x = MathHelper.floor_double(mc.thePlayer.posX);
			int y = MathHelper.floor_double(mc.thePlayer.posY);
			int z = MathHelper.floor_double(mc.thePlayer.posZ);
			for (int i = -6; i < 6; i++) {
				for (int j = -6; j < 6; j++) {
					for (int k = -6; k < 6; k++) {
						int xx = i + x;
						int yy = j + y;
						int zz = k + z;
						Block block = mc.theWorld.getBlockState(new BlockPos(xx, yy, zz)).getBlock();
						if (!block.equals(Blocks.air)
								&& mc.thePlayer.getDistance(xx, yy, zz) <= 6) {
							mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.START_DESTROY_BLOCK, new BlockPos(xx, yy, zz), EnumFacing.DOWN));
							if (!mc.theWorld.func_175719_a(mc.thePlayer, new BlockPos(xx, yy, zz), EnumFacing.UP))
								mc.playerController.func_178888_a(new BlockPos(xx, yy, zz), EnumFacing.UP);
						}
					}
				}
			}
		}
	}

	@EventTarget private void onPreUpdate(EventUpdate event) {
		if (event.type.equals(EnumEventType.PRE)) {
			if (mc.playerController.isInCreativeMode()) {
				return;
			}
			this.pitch = mc.thePlayer.rotationPitch;
			this.yaw = mc.thePlayer.rotationYaw;
			double dist = 4.9D;
			currentms = System.nanoTime() / 1000000L;
			if (mc.thePlayer.isUsingItem()) {
				return;
			}
			if ((nuker != null) && (
					(mc.theWorld.getBlockState(new BlockPos(nuker.getX(), nuker.getY(), nuker.getZ())).getBlock().getMaterial() == Material.air) || (mc.thePlayer.getDistance(nuker.getX(), nuker.getY(), nuker.getZ()) > 6.0D))) {
				nuker = null;
				this.curBlockDamageMP = 0.0F;
			}
			if ((!this.nukerList.isEmpty()) && (nuker == null)) {
				Collections.sort(this.nukerList);
				nuker = (NukerObj) this.nukerList.get(0);
			}
			if ((nuker != null))
				faceBlock(nuker);
		}
	}

	@EventTarget private void onPostUpdate(EventUpdate event) {
		if (event.type.equals(EnumEventType.POST) && !mc.playerController.isInCreativeMode()) {
			double dist = 4.9D;
			if (nuker != null) {
				if (getside(nuker.getX(), nuker.getY(), nuker.getZ()) != null) {
					mc.thePlayer.swingItem();
					if ((!mc.thePlayer.capabilities.isCreativeMode) && (mc.theWorld.getBlockState(new BlockPos(nuker.getX(), nuker.getY(), nuker.getZ())).getBlock().getMaterial() != Material.ground)) {
						mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.START_DESTROY_BLOCK, new BlockPos(nuker.getX(), nuker.getY(), nuker.getZ()), getside(nuker.getX(), nuker.getY(), nuker.getZ()).field_178784_b));
						this.curBlockDamageMP += mc.theWorld.getBlockState(new BlockPos(nuker.getX(), nuker.getY(), nuker.getZ())).getBlock().getPlayerRelativeBlockHardness(mc.thePlayer, mc.theWorld, new BlockPos(nuker.getX(), nuker.getY(), nuker.getZ()));
						if (this.curBlockDamageMP >= 1.0F)
							mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.STOP_DESTROY_BLOCK, new BlockPos(nuker.getX(), nuker.getY(), nuker.getZ()), getside(nuker.getX(), nuker.getY(), nuker.getZ()).field_178784_b));
						//this.mc.playerController.onPlayerDestroyBlock(nuker.getX(), nuker.getY(), nuker.getZ(), getside(nuker.getX(), nuker.getY(), nuker.getZ()).sideHit);
					} else if (mc.theWorld.getBlockState(new BlockPos(nuker.getX(), nuker.getY(), nuker.getZ())).getBlock().getMaterial() == Material.ground) {
						mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.START_DESTROY_BLOCK, new BlockPos(nuker.getX(), nuker.getY(), nuker.getZ()), getside(nuker.getX(), nuker.getY(), nuker.getZ()).field_178784_b));
						mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.STOP_DESTROY_BLOCK, new BlockPos(nuker.getX(), nuker.getY(), nuker.getZ()), getside(nuker.getX(), nuker.getY(), nuker.getZ()).field_178784_b));
						this.curBlockDamageMP += mc.theWorld.getBlockState(new BlockPos(nuker.getX(), nuker.getY(), nuker.getZ())).getBlock().getPlayerRelativeBlockHardness(mc.thePlayer, mc.theWorld, new BlockPos(nuker.getX(), nuker.getY(), nuker.getZ()));
						mc.theWorld.getBlockState(new BlockPos(nuker.getX(), nuker.getY(), nuker.getZ())).getBlock();
						//this.mc.playerController.onBlockDestroyed(nuker.getX(), nuker.getY(), nuker.getZ(), getside(nuker.getX(), nuker.getY(), nuker.getZ()).field_178784_b);
					} else {
						//this.mc.playerController.clickBlock(nuker.getX(), nuker.getY(), nuker.getZ(), getside(nuker.getX(), nuker.getY(), nuker.getZ()));
					}
					last = System.nanoTime() / 1000000L;
				}
			}
			loopBlocks(dist);
			mc.thePlayer.rotationPitch = this.pitch;
			mc.thePlayer.rotationYaw = this.yaw;
		}
	}

	public static float getDistanceBetweenAngles(float par1, float par2) {
		float angle = Math.abs(par1 - par2) % 360.0F;
		if (angle > 180.0F) {
			angle = 360.0F - angle;
		}
		return angle;
	}

	private void loopBlocks(double dist) {
		for (NukerObj o : this.nukerList) {
			if ((mc.thePlayer.getDistance(o.x, o.y, o.z) > dist) || (mc.theWorld.getBlockState(new BlockPos(o.x, o.y, o.z)).getBlock().getMaterial() == Material.air)) {
				this.nukerList.clear();
				break;
			}
		}
		for (int x = -6; x < 6; x++)
			for (int z = -6; z < 6; z++)
				for (int y = 4; y > -2; y--) {
					int blockX = (int) Math.floor(mc.thePlayer.posX + x);
					int blockY = (int) Math.floor(mc.thePlayer.posY + y);
					int blockZ = (int) Math.floor(mc.thePlayer.posZ + z);
					double blockD = mc.thePlayer.getDistance(mc.thePlayer.posX + x, mc.thePlayer.posY + y, mc.thePlayer.posZ + z);
					float yaw = getDistanceBetweenAngles(this.serveryaw, getAngles(blockX, blockY, blockZ)[0]);
					float pitch = getDistanceBetweenAngles(this.serverpitch, getAngles(blockX, blockY, blockZ)[1]);
					float blockF = (yaw + pitch) / 2.0F;
					float blockFD = (float) (blockD / blockF);
					if ((this.nukerList.contains(new NukerObj(blockX, blockY, blockZ, blockFD))) || (blockD > dist) ||
							(mc.theWorld.getBlockState(new BlockPos(blockX, blockY, blockZ)).getBlock().getMaterial() == Material.air))
						continue;
					if (Block.getIdFromBlock(mc.theWorld.getBlockState(new BlockPos(blockX, blockY, blockZ)).getBlock()) == 7) {
						continue;
					}
					this.nukerList.add(new NukerObj(blockX, blockY, blockZ, blockFD));
				}
	}

	private MovingObjectPosition getside(int posX, int posY, int posZ) {
		return mc.theWorld.rayTraceBlocks(new Vec3(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ), new Vec3(posX + 0.5D, posY + 0.5D, posZ + 0.5D));
	}

	public void faceBlock(NukerObj target) {
		double difX = mc.thePlayer.posX - (target.getX() + 0.5D);
		double difY = mc.thePlayer.posY - (target.getY() + 0.5D);
		double difZ = mc.thePlayer.posZ - (target.getZ() + 0.5D);
		double helper = Math.sqrt(difX * difX + difZ * difZ);
		float yaw = (float) (Math.atan2(-difZ, -difX) * 180.0D / 3.141592653589793D) - 90.0F;
		mc.thePlayer.rotationYaw = updateRotation(mc.thePlayer.rotationYaw, yaw, 180.0F);
		this.serveryaw = updateRotation(mc.thePlayer.rotationYaw, yaw, 180.0F);
		float pitch = (float) (-(Math.atan2(-difY, helper) * 180.0D / 3.141592653589793D));
		mc.thePlayer.rotationPitch = updateRotation(mc.thePlayer.rotationPitch, pitch, 90.0F);
		this.serverpitch = updateRotation(mc.thePlayer.rotationPitch, pitch, 90.0F);
	}

	public static boolean canBlockBeSeen(NukerObj o) {
		return mc.theWorld.rayTraceBlocks(new Vec3(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ), new Vec3(o.getX(), o.getY(), o.getZ())) == null;
	}

	private static boolean hasDelayRun(long l) {
		return currentms - last >= l;
	}

	public static float updateRotation(float par1, float par2, float par3) {
		float var4 = MathHelper.wrapAngleTo180_float(par2 - par1);
		if (var4 > par3)
			var4 = par3;
		if (var4 < -par3) {
			var4 = -par3;
		}
		return par1 + var4;
	}

	public static float[] getAngles(int posX, int posY, int posZ) {
		double difX = mc.thePlayer.posX - (posX + 0.5D);
		double difY = mc.thePlayer.posY - (posY + 0.5D);
		double difZ = mc.thePlayer.posZ - (posZ + 0.5D);
		double helper = Math.sqrt(difX * difX + difZ * difZ);
		float yaw = (float) (Math.atan2(difZ, difX) * 180.0D / 3.141592653589793D) - 90.0F;
		float pitch = (float) (-(Math.atan2(-difY, helper) * 180.0D / 3.141592653589793D));
		return new float[]{yaw, pitch};
	}

	class NukerObj implements Comparable<NukerObj> {
		private int x;
		private int y;
		private int z;
		private float f;

		public int hashCode() {
			int prime = 31;
			int result = 1;
			result = 31 * result + getOuterType().hashCode();
			result = 31 * result + this.x;
			result = 31 * result + this.y;
			result = 31 * result + this.z;
			return result;
		}

		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NukerObj other = (NukerObj) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (this.x != other.x)
				return false;
			if (this.y != other.y)
				return false;
			if (this.z != other.z) {
				return false;
			}
			return this.f == other.f;
		}

		public NukerObj(int posX, int posY, int posZ, float blockF) {
			this.x = posX;
			this.y = posY;
			this.z = posZ;
			this.f = blockF;
		}

		public int getX() {
			return this.x;
		}

		public int getY() {
			return this.y;
		}

		public int getZ() {
			return this.z;
		}

		public float getF() {
			return this.f;
		}

		private Nuker getOuterType() {
			return Nuker.this;
		}

		public int compareTo(NukerObj o) {
			if (this.f > o.f)
				return 1;
			if (this.f < o.f) {
				return -1;
			}
			return 0;
		}
	}

}
