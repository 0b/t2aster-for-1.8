package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventTick;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.property.properties.NumberProperty;
import net.minecraft.network.play.client.C03PacketPlayer;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/23/2014
 */
public class Revive extends Plugin {
	private NumberProperty health = new NumberProperty("revive.health", 2);

	public Revive() {
		super("Revive", PluginCategory.COMBAT);
		addProperty(health);
	}

	@EventTarget public void revive(EventTick event) {
		if (mc.thePlayer.getHealth() < health.getValue().floatValue()) {
			// kill yourself
			mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY + 0.1, mc.thePlayer.posZ, true));
			mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY - 1337, mc.thePlayer.posZ, true));
		}
	}
}
