package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventTick;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import org.lwjgl.input.Keyboard;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/18/2014
 */
public class Sneak extends Plugin {
	public Sneak() {
		super("Sneak", PluginCategory.MOVEMENT, Keyboard.KEY_Z);
	}

	@EventTarget public void tick(EventTick eventTick) {
		mc.gameSettings.keyBindSneak.pressed = true;
	}

	@Override public void onDisable() {
		super.onDisable();
		mc.gameSettings.keyBindSneak.pressed = Keyboard.isKeyDown(mc.gameSettings.keyBindSneak.getKeyCode());
	}
}
