package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventUpdate;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.util.EnumEventType;
import com.piegoesmoo.toaster2.util.InventoryUtil;
import com.piegoesmoo.toaster2.util.Timer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemArmor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/23/2014
 */
public class AutoArmor extends Plugin {
	private final Timer timer = new Timer();

	public AutoArmor() {
		super("Auto Armor", PluginCategory.COMBAT);
	}

	@EventTarget public void update(EventUpdate eventUpdate) {
		if (eventUpdate.type.equals(EnumEventType.PRE)) {
			getBestArmor().values().stream().filter(stack -> timer.timePassed(300) && stack != null && stack.getStack() != null && stack.getStack().getItem() != null).forEach(stack -> {
				applyArmor(((ItemArmor) stack.getStack().getItem()).armorType + 5, stack);
			});
		}
	}

	private void applyArmor(int armorType, Slot slot) {
		if ((slot.slotNumber) >= 0) {
			boolean b = mc.thePlayer.getCurrentArmor(armorType - 5) == null;
			InventoryUtil.clickSlot(slot, InventoryUtil.MouseButton.LEFT, false);
			if (b) {
				InventoryUtil.clickSlot(armorType, InventoryUtil.MouseButton.LEFT, true);
			}
			InventoryUtil.clickSlot(armorType, InventoryUtil.MouseButton.LEFT, false);
		}
	}

	private Slot getSlotForPosition(int position) {
		for (Slot slot : (List<Slot>) mc.thePlayer.inventoryContainer.inventorySlots) {
			if (slot.slotNumber == position)
				return slot;
		}

		return null;
	}

	private Map<Integer, Slot> getBestArmor() {
		Map<Integer, Slot> map = new HashMap<>();
		for (int i = 0; i < 4; i++)
			map.put(i, getSlotForPosition(i + 5));

		for (Slot slot : (List<Slot>) mc.thePlayer.inventoryContainer.inventorySlots) {
			if (slot.getStack() == null || slot.slotNumber <= 9) continue;

			if (slot.getStack().getItem() instanceof ItemArmor) {
				ItemArmor armor = (ItemArmor) slot.getStack().getItem();
				boolean b = (map.get(armor.armorType).getStack() == null ||
						armor.getArmorMaterial()
								.compareTo(((ItemArmor) map.get(armor.armorType).getStack().getItem()).getArmorMaterial()) > 0);
				b = b && (mc.thePlayer.getCurrentArmor(armor.armorType) == null
						|| ((ItemArmor) mc.thePlayer.getCurrentArmor(armor.armorType).getItem()).getArmorMaterial().compareTo(armor.getArmorMaterial()) > 0);
				if (b) {
					map.put(armor.armorType, slot);
				}
			}
		}

		return map;
	}
}
