package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.events.EventUpdate;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.property.properties.NumberProperty;
import com.piegoesmoo.toaster2.property.properties.StringProperty;
import com.piegoesmoo.toaster2.util.EnumEventType;
import com.piegoesmoo.toaster2.util.Timer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Spam extends Plugin {
	private List<String> file = new ArrayList<>();
	public StringProperty message = new StringProperty("spam.message", Toaster.clientName.stringValue());
	public NumberProperty delay = new NumberProperty("spam.delay", 1550);
	private final Timer timer = new Timer();

	public Spam() {
		super("Spam", PluginCategory.PLAYER);
		try {
			loadFile();
		} catch (Exception e) {
		}
	}

	@Override
	public void onEnable() {
		try {
			loadFile();
		} catch (Exception e) {
		}
		super.onEnable();
	}

	@EventTarget public void update(EventUpdate eventUpdate) {
		if (eventUpdate.type.equals(EnumEventType.PRE)) {
			boolean textFile = false;
			String fileText = "";
			if (message.getValue().contains("$TEXT")) {
				if (file.isEmpty()) {
					try {
						loadFile();
					} catch (Exception e) {
					}
				}
				textFile = true;
			}
			if (timer.timePassed(delay.getValue().longValue())) {
				if (textFile && (!file.isEmpty())) {
					String s = file.get(0);
					fileText = s;
					file.remove(s);
				}
				if ((textFile && !file.isEmpty()) || !message.getValue().isEmpty()) {
					mc.thePlayer.sendChatMessage(message.getValue().replaceAll("\\$TEXT", fileText));
				}
			}
		}
	}

	public void loadFile() throws IOException {
		File file = new File(Toaster.DATA_DIR, "spam.txt");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (Exception e) {
			}
		}
		BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
		String s;
		while ((s = bufferedReader.readLine()) != null) {
			if ((!s.equals("")) && (!s.equals(" "))) {
				this.file.add(s);
			}
		}
		bufferedReader.close();
	}
}
