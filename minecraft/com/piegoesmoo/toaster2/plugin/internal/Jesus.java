package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.events.EventUpdate;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.util.EnumEventType;
import net.minecraft.init.Blocks;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.BlockPos;
import org.lwjgl.input.Keyboard;

public class Jesus extends Plugin {

	private int ticks;
	public boolean box;

	public Jesus() {
		super("Jesus", PluginCategory.MOVEMENT, Keyboard.KEY_J);
	}

	@EventTarget private void onPreUpdate(EventUpdate e) {
		if (e.type.equals(EnumEventType.PRE)) {
			boolean water = (mc.theWorld.getBlockState(new BlockPos((int) Math.floor(mc.thePlayer.posX), (int) Math.floor(mc.thePlayer.boundingBox.minY) - 1, (int) Math.floor(mc.thePlayer.posZ))).getBlock() == Blocks.water) ||
					(mc.theWorld.getBlockState(new BlockPos((int) Math.floor(mc.thePlayer.posX), (int) Math.floor(mc.thePlayer.boundingBox.minY), (int) Math.floor(mc.thePlayer.posZ))).getBlock() == Blocks.water);
			if (Toaster.getToaster().pluginManager.getPlugin(NoCheat.class).getState()) {
				if (water && !mc.thePlayer.isSneaking()) {
					ticks++;
					if (ticks == 4) {
						mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.boundingBox.minY - 0.01D, mc.thePlayer.posZ, false));
						e.setCancelled(true);
						ticks = 0;
					}
				}
				box = !(mc.thePlayer.isSneaking() || mc.thePlayer.fallDistance >= 3.0F || mc.thePlayer.isInWater());
			} else {
				box = true;
			}
		}
	}

	@Override public void onDisable() {
		ticks = 0;
		box = false;
		super.onDisable();
	}

}
