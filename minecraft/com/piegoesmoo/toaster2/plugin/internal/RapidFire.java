package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventPacket;
import com.piegoesmoo.toaster2.events.EventUpdate;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.util.EnumEventType;
import net.minecraft.init.Items;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemBucketMilk;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemPotion;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C09PacketHeldItemChange;

public class RapidFire extends Plugin{

	public RapidFire() {
		super("RapidFire", PluginCategory.COMBAT);
	}

	@EventTarget private void sendPacket(EventPacket e) {
		if (e.type.equals(EventPacket.PacketType.SEND)) {
			if (mc.thePlayer.getCurrentEquippedItem() == null) return;

			if ((mc.thePlayer.capabilities.isFlying) || (mc.thePlayer.onGround) || 
					(mc.thePlayer.isInWater()) || (mc.thePlayer.isOnLadder())) {
				if ((((mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemBow)) && 
						(mc.thePlayer.inventory.hasItem(Items.arrow)) && (!mc.thePlayer.capabilities.isCreativeMode)) || 
						(((mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemBow)) && 
								(mc.thePlayer.capabilities.isCreativeMode)) || 
								(((mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemFood)) && 
										(mc.thePlayer.capabilities.isCreativeMode)) || 
										(((mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemFood)) && 
												(shouldEat()) && (!mc.thePlayer.capabilities.isCreativeMode)) || 
												((mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemPotion)) || 
												((mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemBucketMilk))) {
					if ((e.getPacket() instanceof C08PacketPlayerBlockPlacement)) {
						C08PacketPlayerBlockPlacement packetBlockPlacement = (C08PacketPlayerBlockPlacement)e.getPacket();
						if (((packetBlockPlacement.getPlacedBlockOffsetX() != -1) && (packetBlockPlacement.getPlacedBlockOffsetY() != -1)) || (packetBlockPlacement.getPlacedBlockOffsetZ() != -1) || (packetBlockPlacement.getPlacedBlockDirection() != 255)) {
							return;
						}
						mc.thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(mc.thePlayer.inventory.currentItem));
						for (int i = 0; i < 400; i++) {
							mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer(false));
						}
						mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging());
					}
				}
			}
		}
	}

	@EventTarget public void update(EventUpdate eventUpdate) {
		if (eventUpdate.type.equals(EnumEventType.PRE)) {
			if (mc.thePlayer.getCurrentEquippedItem() == null) return;
			if (((mc.thePlayer.capabilities.isFlying) || (mc.thePlayer.onGround) || (mc.thePlayer.isInWater()) || (mc.thePlayer.isOnLadder())) && (
					(((mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemBow)) && (mc.thePlayer.inventory.hasItem(Items.arrow)) && (!mc.thePlayer.capabilities.isCreativeMode)) || (((mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemBow)) && (mc.thePlayer.capabilities.isCreativeMode)) || (((mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemFood)) && (mc.thePlayer.capabilities.isCreativeMode)) || (((mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemFood)) && (shouldEat()) && (!mc.thePlayer.capabilities.isCreativeMode)) || ((((mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemPotion)) || ((mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemBucketMilk))) && 
							(mc.gameSettings.keyBindUseItem.pressed))))
				mc.thePlayer.stopUsingItem();
		}
	}

	public boolean shouldEat(){
		if (mc.thePlayer.getFoodStats().getFoodLevel() < 20.0F)
			return true;
		if (mc.thePlayer.getCurrentEquippedItem().getItem() == Items.golden_apple) {
			return true;
		}
		return false;
	}


}
