package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventUpdate;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.util.EnumEventType;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/30/2014
 */
public class NoFall extends Plugin {
	private boolean oldGround;

	public NoFall() {
		super("NoFall", PluginCategory.PLAYER);
	}

	@EventTarget public void update(EventUpdate eventUpdate) {
		if (eventUpdate.type == EnumEventType.PRE) {
			oldGround = mc.thePlayer.onGround;
			mc.thePlayer.onGround = true;
		} else {
			mc.thePlayer.onGround = oldGround;
		}
	}
}
