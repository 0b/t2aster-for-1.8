package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventPacket;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.network.Packet;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.List;

public class Blink extends Plugin {
	private EntityOtherPlayerMP blinkPlayer;
	private final List<Packet> packets = new ArrayList<>();

	public Blink() {
		super("Blink", PluginCategory.MOVEMENT, Keyboard.KEY_H);
	}

	@Override public void onEnable() {
		super.onEnable();
		blinkPlayer = new EntityOtherPlayerMP(mc.theWorld,
				mc.thePlayer.getGameProfile());
		blinkPlayer.copyLocationAndAnglesFrom(mc.thePlayer);
		blinkPlayer.inventory = mc.thePlayer.inventory;
		mc.theWorld.addEntityToWorld(-666, blinkPlayer);
	}

	@EventTarget private void sendPacket(EventPacket e) {
		if (e.type.equals(EventPacket.PacketType.SEND)) {
			this.packets.add(e.getPacket());
			e.setCancelled(true);
		}
	}

	@Override public void onDisable() {
		super.onDisable();
		mc.theWorld.removeEntityFromWorld(-666);
		blinkPlayer = null;
		if (!packets.isEmpty())
			packets.stream().filter(p -> p != null).forEach(mc.thePlayer.sendQueue::addToSendQueue);
		this.packets.clear();
	}

}
