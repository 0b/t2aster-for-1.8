package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventIngameRender;
import com.piegoesmoo.toaster2.plugin.Plugin;
import net.minecraft.client.gui.FontRenderer;
import org.lwjgl.opengl.GL11;

public class NoCheat extends Plugin {

	public NoCheat() {
		super("NoCheat");
	}

	@EventTarget public void render(EventIngameRender eventIngameRender) {
		FontRenderer f = mc.fontRendererObj;
		GL11.glEnable(GL11.GL_BLEND);
		f.drawStringWithShadow("NoCheat", 2, 11, 0x88FFFFFF);
		GL11.glDisable(GL11.GL_BLEND);
	}

}
