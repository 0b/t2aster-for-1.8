package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventUpdate;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.util.EnumEventType;
import net.minecraft.init.Blocks;
import net.minecraft.util.BlockPos;

public class Speed extends Plugin {

	private int speedUpdate;
	private static boolean onground;
	private int ongroundtick;

	public Speed() {
		super("Gotta Go Fast", PluginCategory.MOVEMENT);
	}

	@EventTarget private void onPreUpdate(EventUpdate event) {
		if (event.type.equals(EnumEventType.PRE)) {
			boolean water = (mc.theWorld.getBlockState(new BlockPos((int) Math.floor(mc.thePlayer.posX), (int) Math.floor(mc.thePlayer.boundingBox.minY) - 1, (int) Math.floor(mc.thePlayer.posZ))).getBlock() == Blocks.water) ||
					(mc.theWorld.getBlockState(new BlockPos((int) Math.floor(mc.thePlayer.posX), (int) Math.floor(mc.thePlayer.boundingBox.minY), (int) Math.floor(mc.thePlayer.posZ))).getBlock() == Blocks.water);
			boolean strafe = mc.thePlayer.moveStrafing != 0.0F;
			onground = isOnground(5);
			if (mc.thePlayer.fallDistance <= 0.0F) {
				//mc.thePlayer.motionY = -6.0D;
			}
			if ((!mc.thePlayer.isSneaking()) && ((mc.thePlayer.moveForward != 0.0F) ||
					(mc.thePlayer.moveStrafing != 0.0F)) && (!mc.thePlayer.isOnLadder()) &&
					(!mc.thePlayer.isCollidedHorizontally) && (!water) && (mc.thePlayer.onGround || !mc.thePlayer.isAirBorne)) {
				double e = mc.thePlayer.isSprinting() ? 0.021D : 0.4D;
				double r = strafe ? 2.62D : 2.65D;
				double a = r + e;
				if (onground) {
					if (speedUpdate == 0) {
						speedUpdate++;
						mc.timer.timerSpeed = 1.1F;
						mc.thePlayer.motionX *= a;
						mc.thePlayer.motionZ *= a;
						mc.thePlayer.setPosition(mc.thePlayer.posX, mc.thePlayer.posY + 0.01, mc.thePlayer.posZ);
					} else if (speedUpdate >= 1) {
						mc.thePlayer.setPosition(mc.thePlayer.posX, mc.thePlayer.posY + 0.002, mc.thePlayer.posZ);
						mc.timer.timerSpeed = 1.0F;
						mc.thePlayer.motionX /= 1.5F;
						mc.thePlayer.motionZ /= 1.5F;
						speedUpdate = 0;
					}
				} else {
					mc.timer.timerSpeed = 1.0F;
				}
			}
		}
	}

	private boolean isOnground(int i) {
		if ((ongroundtick <= i) && (mc.thePlayer.onGround) && (!mc.thePlayer.capabilities.allowFlying)) {
			if (ongroundtick < i)
				ongroundtick++;
			if (ongroundtick == i)
				return true;
		}
		if (((!mc.thePlayer.onGround) || (ongroundtick > i)) && (!mc.thePlayer.capabilities.allowFlying)) {
			ongroundtick = 0;
			return false;
		}
		return false;
	}
}
