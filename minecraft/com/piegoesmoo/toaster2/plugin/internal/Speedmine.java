package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventBlockClick;
import com.piegoesmoo.toaster2.events.EventTick;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import net.minecraft.block.Block;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import org.lwjgl.input.Mouse;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/1/2014
 */
public class Speedmine extends Plugin {
	private int prevSlot = Integer.MIN_VALUE;
	private boolean switched = false;

	public Speedmine() {
		super("Speedmine", PluginCategory.WORLD);
	}

	@EventTarget public void damage(EventTick eventTick) {
		if (!Mouse.isButtonDown(0) && switched) {
			switched = false;
			mc.thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(prevSlot));
		}
		if (switched)
			mc.playerController.curBlockDamageMP += 0.18;
		//mc.playerController.blockHitDelay = 0;
	}

	@EventTarget public void onClick(EventBlockClick e) {
		if (!mc.playerController.isInCreativeMode()) {
			prevSlot = mc.thePlayer.inventory.currentItem;
			Block b = mc.theWorld.getBlockState(e.getBlockPos()).getBlock();
			float damage = 1;
			int slot = -1;
			float dambuf;
			for (int i = 0; i < mc.thePlayer.inventory.mainInventory.length; i++) {
				if (mc.thePlayer.inventory.mainInventory[i] == null)
					continue;
				if ((dambuf = mc.thePlayer.inventory.mainInventory[i].getStrVsBlock(b)) > damage) {
					damage = dambuf;
					slot = i;
				}
			}

			if (slot == -1) {
				return;
			}

			switched = true;

			if (slot < 9) mc.thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(slot));
			else mc.playerController.windowClick(0, slot, mc.thePlayer.inventory.currentItem, 2, mc.thePlayer);
		}
	}
}
