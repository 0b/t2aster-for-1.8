package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.cmd.TCommand;
import com.piegoesmoo.toaster2.events.EventIngameRender;
import com.piegoesmoo.toaster2.events.EventTick;
import com.piegoesmoo.toaster2.hook.TGuiIngame;
import com.piegoesmoo.toaster2.plugin.Plugin;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import java.awt.*;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/25/2014
 */
public class FourTwenty extends Plugin {
	private static final ResourceLocation SMOKE_WEED_EVERY_DAY = new ResourceLocation("toaster/smoke_weed_every_day.png");
	private int rotate = 180;
	public static Color drugs;
	private float h = 0;

	public FourTwenty() {
		super("FourTwenty");

		Toaster.getToaster().commandManager.addCommand(new TCommand("420") {
			@Override public void runCommand(String... args) {
				FourTwenty.this.toggle();
			}
		});
	}

	@EventTarget public void render(EventIngameRender eventIngameRender) {
		GL11.glPushMatrix();
		GL11.glScaled(0.25, 0.25, 0.25);
		if (rotate >= 360)
			rotate = 0;
		GL11.glTranslated(388, 469, 0);
		GL11.glRotatef(-(rotate += 1), 0, 0, 1);
		GL11.glTranslated(-388, -469, 0);

		TGuiIngame.drawTexturedRectangle(SMOKE_WEED_EVERY_DAY, 80, 80, 594, 499, 0, 0, 594, 599, 594, 599);
		GL11.glPopMatrix();
		mc.thePlayer.timeInPortal = 0.5F;

		String weed = "SMOKE WEED EVERY DAY";
		GL11.glPushMatrix();
		GL11.glScaled(2, 2, 2);
		GL11.glTranslated(eventIngameRender.getScaledResolution().getScaledWidth() / 4, eventIngameRender.getScaledResolution().getScaledHeight() / 4, 0);
		GL11.glRotatef(rotate, 0, 0, 1);
		GL11.glTranslated(-(eventIngameRender.getScaledResolution().getScaledWidth() / 4), -(eventIngameRender.getScaledResolution().getScaledHeight() / 4), 0);

		mc.fontRendererObj.drawStringWithShadow(weed, eventIngameRender.getScaledResolution().getScaledWidth() / 4 - mc.fontRendererObj.getStringWidth(weed) / 2, eventIngameRender.getScaledResolution().getScaledHeight() / 4 - mc.fontRendererObj.FONT_HEIGHT / 4, 0xFFFFFF);

		GL11.glPopMatrix();
	}

	@EventTarget public void tick(EventTick eventTick) {
		drugs = Color.getHSBColor(h, 1, 1);
		if (h < 1F) {
			h += 0.01F;
		} else h = 0F;
	}
}
