package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.Event3DRender;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.util.EnumEventType;
import com.piegoesmoo.toaster2.util.GLUtil;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.AxisAlignedBB;

import java.awt.*;

import static org.lwjgl.opengl.GL11.*;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/3/2014
 */
public class ChestESP extends Plugin {
	private float h = 0;
	private Color rainbow = new Color(0);

	public ChestESP() {
		super("Chest ESP", PluginCategory.RENDER);
	}

	@EventTarget
	public void onRender3D(Event3DRender e) {
		if (e.getType().equals(EnumEventType.PRE)) {
			glPushAttrib(GL_ALL_ATTRIB_BITS);
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_LIGHTING);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glLineWidth(3);
			glEnable(GL_LINE_SMOOTH);
			glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
			glEnable(GL_STENCIL_TEST);
			glClear(GL_STENCIL_BUFFER_BIT);
			glClearStencil(0xF);
			glStencilFunc(GL_NEVER, 1, 0xF);
			glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			render();
			glStencilFunc(GL_NEVER, 0, 0xF);
			glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			render();
			glStencilFunc(GL_EQUAL, 1, 0xF);
			glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			glEnable(GL_POLYGON_OFFSET_LINE);
			glPolygonOffset(1, -1000000);
			render();
			glPolygonOffset(1, 1000000);
			glDisable(GL_POLYGON_OFFSET_LINE);
			glDisable(GL_STENCIL_TEST);
			glDisable(GL_LINE_SMOOTH);
			glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
			glDisable(GL_BLEND);
			glEnable(GL_LIGHTING);
			glEnable(GL_TEXTURE_2D);
			glPopAttrib();
		}
	}

	public void render() {
		h += 0.001F;
		if (h > 1F)
			h = 0;

		rainbow = Color.getHSBColor(h, 1F, 1F);
		mc.theWorld.loadedTileEntityList.stream().filter(o -> o instanceof TileEntityChest).forEach(o -> {
			final TileEntityChest chest = (TileEntityChest) o;
			final double renderX = chest.getPos().getX() - mc.getRenderManager().viewerPosX;
			final double renderY = chest.getPos().getY() - mc.getRenderManager().viewerPosY;
			final double renderZ = chest.getPos().getZ() - mc.getRenderManager().viewerPosZ;
			glPushMatrix();
			glTranslated(renderX, renderY, renderZ);
			GLUtil.setColor(rainbow);
			if (chest.adjacentChestXPos != null) {
				drawBox(AxisAlignedBB.fromBounds(0.0625D, 0.0D, 0.0625D, 1.9375, 0.875F, 0.9375F));
			} else if (chest.adjacentChestZPos != null) {
				drawBox(AxisAlignedBB.fromBounds(0.0625F, 0.0D, 0.0625D, 0.9375D, 0.875F, 1.9375));
			} else if ((chest.adjacentChestXNeg == null) && (chest.adjacentChestZNeg == null)) {
				drawBox(AxisAlignedBB.fromBounds(0.0625F, 0.0F, 0.0625D, 0.9375F, 0.875F, 0.9375F));
			}
			glPopMatrix();
		});
	}

	public void renderAlt() {
		h += 0.001F;
		if (h > 1F)
			h = 0;

		rainbow = Color.getHSBColor(h, 1F, 1F);
		mc.theWorld.loadedTileEntityList.stream().filter(o -> o instanceof TileEntityChest).forEach(o -> {
			final TileEntityChest chest = (TileEntityChest) o;
			final double renderX = chest.getPos().getX() - mc.getRenderManager().viewerPosX;
			final double renderY = chest.getPos().getY() - mc.getRenderManager().viewerPosY;
			final double renderZ = chest.getPos().getZ() - mc.getRenderManager().viewerPosZ;
			glPushMatrix();
			glTranslated(renderX, renderY, renderZ);
			GLUtil.setColor(rainbow);
			if (chest.adjacentChestXPos != null) {
				drawBox(AxisAlignedBB.fromBounds(0, 0.0D, 0, 2, 1, 1));
			} else if (chest.adjacentChestZPos != null) {
				drawBox(AxisAlignedBB.fromBounds(0, 0.0D, 0, 1, 1, 2));
			} else if ((chest.adjacentChestXNeg == null) && (chest.adjacentChestZNeg == null)) {
				drawBox(AxisAlignedBB.fromBounds(0, 0.0F, 0, 1, 1, 1));
			}
			glPopMatrix();
		});
	}

	public static void drawBox(AxisAlignedBB boundingBox) {
		if (boundingBox == null) {
			return;
		}
		glBegin(7);
		glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		glEnd();
		glBegin(7);
		glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		glEnd();

		glBegin(7);
		glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		glEnd();
		glBegin(7);
		glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		glEnd();

		glBegin(7);
		glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		glEnd();
		glBegin(7);
		glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		glEnd();

		glBegin(7);
		glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		glEnd();
		glBegin(7);
		glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		glEnd();

		glBegin(7);
		glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		glEnd();

		glBegin(7);
		glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		glEnd();

		glBegin(7);
		glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		glEnd();

		glBegin(7);
		glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		glEnd();
	}
}
