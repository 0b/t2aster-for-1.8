package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventTick;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import org.lwjgl.input.Keyboard;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/27/2014
 */
public class Brightness extends Plugin {
	private float oldBright;

	public Brightness() {
		super("Brightness", PluginCategory.RENDER, Keyboard.KEY_C);
	}

	@Override
	public void onEnable() {
		oldBright = mc.gameSettings.gammaSetting;
		super.onEnable();
	}

	@Override
	public void onDisable() {
		super.onDisable();
		if (oldBright == 20F) {
			oldBright = 0;
		}
		mc.gameSettings.gammaSetting = oldBright;
	}

	@Override
	public void unload() {
		if (this.getState())
			if (oldBright == 20F) {
				oldBright = 0;
			}
		mc.gameSettings.gammaSetting = oldBright;
		super.unload();
	}

	@EventTarget
	public void tick(EventTick eventTick) {
		mc.theWorld.setRainStrength(0);
		mc.gameSettings.gammaSetting = 20F;
	}
}
