package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.events.Event3DRender;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.util.EnumEventType;
import com.piegoesmoo.toaster2.util.GLUtil;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.player.EntityPlayer;

import static org.lwjgl.opengl.GL11.*;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/6/2014
 */
public class ESP extends Plugin {
	public ESP() {
		super("ESP", PluginCategory.RENDER);
	}

	@EventTarget public void render3D(Event3DRender dRender) {
		if (!dRender.getType().equals(EnumEventType.POST)) return;
		glPushMatrix();
		glTranslated(0, mc.thePlayer.getEyeHeight(), 0);
		glEnable(GL_ALPHA_TEST);
		for (Object o : mc.theWorld.loadedEntityList) {
			if ((o instanceof EntityPlayer)) {
				if (!(o instanceof EntityPlayerSP)) {
					EntityPlayer ep = (EntityPlayer) o;
					double x = ep.posX - mc.getRenderManager().viewerPosX;
					double y = ep.posY - mc.getRenderManager().viewerPosY - ep.getEyeHeight();
					double z = ep.posZ - mc.getRenderManager().viewerPosZ;
					String playerName = ep.getCommandSenderEntity().getName();
					double dist = mc.thePlayer.getDistanceToEntity(ep);
					if (playerName.equals("piegoesmoo") || playerName.equals("tojatta")) {
						glColor4f(1, 0, 1, 1);
					} else if (Toaster.getToaster().profileManager.isFriend(playerName)) {
						GLUtil.setColor(Toaster.FRIEND_COLOR);
					} else if (dist >= 96) {
						glColor4f(0F, 1F, 0F, 0.75F);
					} else {
						glColor4d(1, dist / 64, 0, 0.75);
					}
					glBegin(2);
					glVertex3d(0.0D, 0.0D, 0.0D);
					glVertex3d(x, y, z);
					glEnd();
				}
			}
		}
		glDisable(GL_ALPHA_TEST);
		glPopMatrix();
	}
}
