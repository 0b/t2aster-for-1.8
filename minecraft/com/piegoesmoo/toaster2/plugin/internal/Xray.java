package com.piegoesmoo.toaster2.plugin.internal;

import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/19/2014
 */
public class Xray extends Plugin {
	public static final List<Block> xrayBlocks = new ArrayList<>();

	public Xray() {
		super("Xray", PluginCategory.WORLD, Keyboard.KEY_X);

		Collections.addAll(xrayBlocks,
				Blocks.gold_block,
				Blocks.gold_ore,
				Blocks.diamond_block,
				Blocks.diamond_ore,
				Blocks.iron_block);
	}

	@Override public void toggle() {
		super.toggle();
		mc.renderGlobal.loadRenderers();
	}
}
