package com.piegoesmoo.toaster2.plugin.internal;

import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventTick;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.property.properties.BoolProperty;
import net.minecraft.init.Blocks;
import org.lwjgl.input.Keyboard;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/20/2014
 */
public class Sprint extends Plugin {
	private BoolProperty fastIce = new BoolProperty("ice.fast", true);

	public Sprint() {
		super("Sprint", PluginCategory.MOVEMENT, Keyboard.KEY_V);
	}

	@EventTarget
	public void tick(EventTick eventTick) {
		float iceSlipperiness = (mc.thePlayer.capabilities.allowFlying || fastIce.getValue()) ? 0.98F : 0.4F;
		Blocks.ice.slipperiness = iceSlipperiness;
		Blocks.packed_ice.slipperiness = iceSlipperiness;

		mc.thePlayer.setSprinting(Keyboard.isKeyDown(mc.gameSettings.keyBindForward.getKeyCode())
				&& mc.inGameHasFocus
				&& !mc.thePlayer.isCollidedHorizontally
				&& (mc.thePlayer.getFoodStats().getFoodLevel() > 6 || mc.playerController.isInCreativeMode())
				&& !mc.thePlayer.isSneaking()
				&& !mc.thePlayer.capabilities.isFlying);
	}
}
