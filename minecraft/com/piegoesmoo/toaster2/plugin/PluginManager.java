package com.piegoesmoo.toaster2.plugin;

import com.google.common.reflect.ClassPath;
import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.cmd.TCommand;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/20/2014
 */
public class PluginManager {
	private static final File PLUGIN_DIRECTORY = new File(Toaster.DATA_DIR, "plugins");

	private final List<Plugin> pluginList = new ArrayList<>();

	public PluginManager() {
	}

	public void init() {
		// Load internal plugins
		try {
			for (ClassPath.ClassInfo classInfo : ClassPath.from(Plugin.class.getClassLoader()).getTopLevelClassesRecursive("com.piegoesmoo.toaster2.plugin.internal")) {
				Class<?> clazz = classInfo.load();
				if (Plugin.class.isAssignableFrom(clazz))
					pluginList.add((Plugin) clazz.newInstance());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Does what it says!
		loadExternalPlugins();

		// Pretty up the list by sorting alphabetically.
		Collections.sort(pluginList, (o1, o2) -> o1.getName().compareTo(o2.getName()));

		// Add the command that lists all plugins
		Toaster.getToaster().commandManager.addCommand(new TCommand("plugins") {
			@Override
			public void runCommand(String... args) {
				String s = "Plugins loaded (" + pluginList.size() + "): ";
				for (Plugin p : pluginList) {
					s += p.getName() + ", ";
				}

				s = s.substring(0, s.length() - 2);
				Toaster.CHAT_OUTPUT.log(s);
			}
		});
	}

	private void loadExternalPlugins() {
		// Make sure the plugins directory exists and is, in fact, a directory
		if (PLUGIN_DIRECTORY.exists() && PLUGIN_DIRECTORY.isDirectory()) {
			try {
				// Check if the directory has items in it
				if (PLUGIN_DIRECTORY.listFiles().length > 0) {
					// Loop through all items in plugins directory
					for (File file : PLUGIN_DIRECTORY.listFiles()) {
						// Ensure that it is a jar file.
						if (file.getName().endsWith(".jar")) {
							// Create JarFile instance and loop through all of the items in the jar
							JarFile jarFile = new JarFile(file);
							// URLClassLoader for loading classes inside the jar.
							URLClassLoader cLoader = new URLClassLoader(new URL[]{file.toURI().toURL()}, this.getClass().getClassLoader());
							// This list makes life easier
							List<JarEntry> entries = Collections.list(jarFile.entries());
							// Ensures the plugin jar contains plugin.xml; required.
							if (containsPluginInfo(entries)) {
								entries.stream().filter(entry -> !entry.isDirectory()).forEach(entry -> {
									if (entry.getName().endsWith(".class")) {
										// Make the path a valid class name
										// (e.g. net/minecraft/client/Minecraft.class -> net.minecraft.client.Minecraft)
										String className = entry.getName().replaceAll("/", ".").substring(0, entry.getName().length() - 6);
										// New try statement for more specific errors
										try {
											// Load class
											Class<?> clazz = cLoader.loadClass(className);
											// Check if it is a plugin
											if (Plugin.class.isAssignableFrom(clazz)) {
												// Add the plugin to the plugins list
												pluginList.add((Plugin) clazz.newInstance());
												// Tell me about it :P
												System.out.printf("Loaded external plugin \"%s\"\n", clazz.getSimpleName());
											}
										} catch (Exception e) {
											// oh noes
											System.err.println("Could not instantiate " + className);
											e.printStackTrace();
										}
									} else if (entry.getName().equalsIgnoreCase("plugin.xml")) {
										//entry.
									}
								});
							}
							jarFile.close();
							cLoader.close();
						}
					}
				}
			} catch (Exception e) {
				// oh no
				e.printStackTrace();
			}
		} else {
			// Make the plugins dir if it doesn't already exist
			PLUGIN_DIRECTORY.mkdir();
		}
	}

	public Plugin getPlugin(String name) {
		for (Plugin p : pluginList) {
			if (p.getName().replaceAll(" ", "").equalsIgnoreCase(name))
				return p;
		}

		return null;
	}

	public <T extends Plugin> T getPlugin(Class<T> clazz) {
		for (Plugin p : pluginList) {
			if (p.getClass().equals(clazz))
				return (T) p;
		}

		return null;
	}

	public List<Plugin> getPluginList() {
		return pluginList;
	}

	private boolean containsPluginInfo(List<JarEntry> entries) {
		boolean contains = false;
		for (JarEntry entry : entries) {
			contains = contains || entry.getName().equals("plugin.xml");
		}
		return contains;
	}
}
