package com.piegoesmoo.toaster2.hook;

import com.darkmagician6.eventapi.EventManager;
import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.events.EventMove;
import com.piegoesmoo.toaster2.events.EventUpdate;
import com.piegoesmoo.toaster2.irc.IRCHandler;
import com.piegoesmoo.toaster2.plugin.internal.AntiVelocity;
import com.piegoesmoo.toaster2.plugin.internal.Flight;
import com.piegoesmoo.toaster2.plugin.internal.Freecam;
import com.piegoesmoo.toaster2.util.EnumEventType;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.stats.StatFileWriter;
import net.minecraft.world.World;
import org.lwjgl.input.Keyboard;

import java.util.Random;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/27/2014
 */
public class TEntityPlayerSP extends EntityPlayerSP {
	private final Minecraft mc;
	private final Random random = new Random();

	public TEntityPlayerSP(Minecraft mcIn, World worldIn, NetHandlerPlayClient p_i46278_3_, StatFileWriter p_i46278_4_) {
		super(mcIn, worldIn, p_i46278_3_, p_i46278_4_);
		mc = mcIn;
	}

	@Override public void func_175161_p() {
		if (!EventManager.call(new EventUpdate(EnumEventType.PRE)).isCancelled())
			super.func_175161_p();
		EventManager.call(new EventUpdate(EnumEventType.POST));

	}

	@Override public void sendChatMessage(String msg) {
		if (msg.startsWith("@")) {
			if (Toaster.getToaster().ircHandler == null) {
				new Thread("IRC Thread") {
					@Override public void run() {
						Toaster.CHAT_OUTPUT.log("Connecting to IRC...");
						String pw, un;
						if ((pw = IRCHandler.loadInfo()) != null && ((un = mc.thePlayer.getCommandSenderEntity().getName()) != null)) {
							Toaster.getToaster().ircHandler = new IRCHandler(un, pw);
							Toaster.getToaster().ircHandler.init();
						} else {
							Toaster.CHAT_OUTPUT.log("You have not registered for IRC! Please use .irc register <password>");
						}
					}
				}.start();
				return;
			}
			Toaster.getToaster().ircHandler.sendMessage(msg.substring(1));
			return;
		}
		if (!Toaster.getToaster().commandManager.runCommand(msg)) {
			msg = msg.replaceAll("\\$RAND", String.valueOf(random.nextInt(10000)))
					.replaceAll("\\$ME", this.getName());
			super.sendChatMessage(msg);
		}
	}

	@Override public void setVelocity(double p_70016_1_, double p_70016_3_, double p_70016_5_) {
		if (!Toaster.getToaster().pluginManager.getPlugin(AntiVelocity.class).getState())
			super.setVelocity(p_70016_1_, p_70016_3_, p_70016_5_);
	}

	@Override public void addVelocity(double p_70024_1_, double p_70024_3_, double p_70024_5_) {
		if (!Toaster.getToaster().pluginManager.getPlugin(AntiVelocity.class).getState())
			super.addVelocity(p_70024_1_, p_70024_3_, p_70024_5_);
	}

	@Override public void moveEntity(double x, double y, double z) {
		if ((mc.thePlayer.isInWater() || mc.thePlayer.isInsideOfMaterial(Material.lava)) && y < 0
				&& Keyboard.isKeyDown(mc.gameSettings.keyBindSneak.getKeyCode())) {
			y -= 1;
		}
		EventMove eventMove = EventManager.call(new EventMove());
		super.moveEntity(x * eventMove.gethMod(), y * eventMove.getvMod(), z * eventMove.gethMod());
	}

	@Override protected boolean pushOutOfBlocks(double x, double y, double z) {
		return !Toaster.getToaster().pluginManager.getPlugin(Freecam.class).getState() && super.pushOutOfBlocks(x, y, z);
	}

	@Override public boolean isEntityInsideOpaqueBlock() {
		return (!Toaster.getToaster().pluginManager.getPlugin(Freecam.class).getState()) && super.isEntityInsideOpaqueBlock();
	}

	@Override public boolean func_175149_v() {
		return (Toaster.getToaster().pluginManager.getPlugin(Freecam.class).getState()
				&& Toaster.getToaster().pluginManager.getPlugin(Flight.class).getState())
				&& !super.func_175149_v();
	}
}
