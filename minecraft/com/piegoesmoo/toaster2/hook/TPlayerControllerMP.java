package com.piegoesmoo.toaster2.hook;

import com.darkmagician6.eventapi.EventManager;
import com.piegoesmoo.toaster2.events.EventBlockClick;
import com.piegoesmoo.toaster2.util.EnumEventType;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/17/2014
 */
public class TPlayerControllerMP extends PlayerControllerMP {
	private final Minecraft mc;

	public TPlayerControllerMP(Minecraft mcIn, NetHandlerPlayClient p_i45062_2_) {
		super(mcIn, p_i45062_2_);
		mc = mcIn;
	}

	public boolean func_180511_b(BlockPos blockPos, EnumFacing enumFacing) {
		EventManager.call(new EventBlockClick(blockPos, enumFacing, EnumEventType.PRE));
		boolean b = super.func_180511_b(blockPos, enumFacing);
		EventManager.call(new EventBlockClick(blockPos, enumFacing, EnumEventType.POST));
		return b;
	}

	@Override public float getBlockReachDistance() {
		return super.getBlockReachDistance();
	}
}
