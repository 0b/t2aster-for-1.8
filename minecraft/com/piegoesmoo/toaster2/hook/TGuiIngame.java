package com.piegoesmoo.toaster2.hook;

import com.darkmagician6.eventapi.EventManager;
import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.events.EventIngameRender;
import com.piegoesmoo.toaster2.plugin.Plugin;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import static org.lwjgl.opengl.GL11.*;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/20/2014
 */
public class TGuiIngame extends GuiIngame {
	private final Minecraft mc;
	public static ScaledResolution scaledResolution;
	public static final ResourceLocation logoLoc = new ResourceLocation("toaster/ToasterLogo.png");

	public TGuiIngame(Minecraft p_i1036_1_) {
		super(p_i1036_1_);
		mc = p_i1036_1_;
	}

	@Override
	public void func_175180_a(float p_73830_1_) {
		super.func_175180_a(p_73830_1_);
		FontRenderer f = mc.fontRendererObj;
		scaledResolution = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);
		if (!mc.gameSettings.showDebugInfo) {
			GL11.glPushMatrix();
			f.setUnicodeFlag(false);
			GL11.glEnable(GL11.GL_BLEND);
			f.drawStringWithShadow(Toaster.clientName.stringValue(), 2, 2, 0x88FFFFFF);
			GL11.glDisable(GL11.GL_BLEND);

			int yPos = 2;
			for (Plugin p : Toaster.getToaster().pluginManager.getPluginList()) {
				if (p.getState()) {
					int col = p.getCategory().getColor();
					if (col == -1)
						continue;
					f.drawStringWithShadow(p.getName(), scaledResolution.getScaledWidth() - f.getStringWidth(p.getName()) - 2, yPos, col);
					yPos += 10;
				}
			}
			GL11.glPopMatrix();

			if (mc.inGameHasFocus)
				Toaster.getToaster().keybindManager.getGuiManager().renderPinned();

			EventIngameRender.EVENT_INGAME_RENDER.setScaledResolution(scaledResolution);
			EventManager.call(EventIngameRender.EVENT_INGAME_RENDER);
		}


	}

	public static void drawTexturedRectangle(ResourceLocation resourceLocation, float x, float y, float width, float height,
	                                         float u, float v, float uWidth, float vHeight, int textureWidth, int textureHeight) {
		glEnable(GL_BLEND);
		Minecraft.getMinecraft().getTextureManager().bindTexture(resourceLocation);
		//glColor4f(1f, 1f, 1f, 1f);
		glBegin(GL_QUADS);
		glTexCoord2d(u / textureWidth, v / textureHeight);
		glVertex2d(x, y);
		glTexCoord2d(u / textureWidth, (v + vHeight) / textureHeight);
		glVertex2d(x, y + height);
		glTexCoord2d((u + uWidth) / textureWidth, (v + vHeight) / textureHeight);
		glVertex2d(x + width, y + height);
		glTexCoord2d((u + uWidth) / textureWidth, v / textureHeight);
		glVertex2d(x + width, y);
		glEnd();
		glDisable(GL_BLEND);
	}
}
