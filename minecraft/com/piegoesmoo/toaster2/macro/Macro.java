package com.piegoesmoo.toaster2.macro;

import com.piegoesmoo.toaster2.keybind.Bindable;
import net.minecraft.client.Minecraft;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/13/2014
 */
public class Macro extends Bindable {
	private String message;

	public Macro(String message, int keybind) {
		this.message = message;
		this.setKeybind(keybind);
	}

	@Override public void fire() {
		Minecraft.getMinecraft().thePlayer.sendChatMessage(message);
	}

	public String getMessage() {
		return message;
	}
}
