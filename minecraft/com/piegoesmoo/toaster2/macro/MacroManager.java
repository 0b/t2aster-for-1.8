package com.piegoesmoo.toaster2.macro;

import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.keybind.Bindable;
import com.piegoesmoo.toaster2.util.TFile;

import java.io.*;
import java.util.Iterator;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/13/2014
 */
public class MacroManager {
	private static final File MACRO_FILE = new TFile("macros");

	public void init() {
		if (MACRO_FILE.exists()) {
			try (BufferedReader inputStream = new BufferedReader(new FileReader(MACRO_FILE))) {
				String line;
				while ((line = inputStream.readLine()) != null) {
					if (line.contains(":")) {
						String[] split = line.split(":");
						Toaster.getToaster().keybindManager.addKeybindable(new Macro(split[1], Integer.parseInt(split[0])));
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			try {
				MACRO_FILE.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void saveMacros() {
		try (BufferedWriter outputStream = new BufferedWriter(new FileWriter(MACRO_FILE))) {
			for (Bindable k : Toaster.getToaster().keybindManager.getBindables())
				if (k instanceof Macro) {
					outputStream.write(k.getKeybind() + ":" + ((Macro) k).getMessage() + "\r\n");
				}
			outputStream.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addMacro(Macro m) {
		Toaster.getToaster().keybindManager.addKeybindable(m);
	}

	public void removeMacro(int key) {
		Iterator<Bindable> i = Toaster.getToaster().keybindManager.getBindables().iterator();
		while (i.hasNext()) {
			Bindable k = i.next();
			if (k.getKeybind() == key)
				i.remove();
		}
	}
}
