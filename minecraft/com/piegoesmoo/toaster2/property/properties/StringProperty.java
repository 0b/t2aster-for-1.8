package com.piegoesmoo.toaster2.property.properties;

import com.piegoesmoo.toaster2.property.Property;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/3/2014
 */
public class StringProperty extends Property<String> {
	public StringProperty(String name, String value) {
		super(name, value);
	}

	@Override
	public String fromString(String from) {
		return (value = from);
	}

	@Override
	public String stringValue() {
		return value;
	}
}
