package com.piegoesmoo.toaster2.property.properties;

import com.piegoesmoo.toaster2.property.Property;

import java.util.regex.Pattern;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/3/2014
 */
public class NumberProperty extends Property<Number> {
	private static final Pattern PATTERN = Pattern.compile("[a-zA-Z](?![.',&])\\p{Punct}");

	public NumberProperty(String name, Number value) {
		super(name, value);
	}

	@Override
	public Number fromString(String from) throws Exception {
		if (PATTERN.matcher(from).matches())
			throw new Exception("Invalid Number: " + from);

		return value = from.contains(".") ? Double.parseDouble(from) : Integer.parseInt(from);
	}

	@Override
	public String stringValue() {
		return String.valueOf(value);
	}
}
