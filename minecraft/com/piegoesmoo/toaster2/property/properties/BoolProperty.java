package com.piegoesmoo.toaster2.property.properties;

import com.piegoesmoo.toaster2.property.Property;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/3/2014
 */
public class BoolProperty extends Property<Boolean> {
	public BoolProperty(String name, Boolean value) {
		super(name, value);
	}

	@Override
	public Boolean fromString(String from) throws Exception {
		if(from == null || (
				!from.equalsIgnoreCase("true")
				&& !from.equalsIgnoreCase("false")))
			throw new Exception("Invalid boolean string: " + from);
		return value = Boolean.parseBoolean(from);
	}

	@Override
	public String stringValue() {
		return getValue() ? "true" : "false";
	}
}
