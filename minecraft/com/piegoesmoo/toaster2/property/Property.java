package com.piegoesmoo.toaster2.property;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/1/2014
 */
public abstract class Property<T> {
	protected T value;
	private final String name;

	public Property(String name, T value) {
		this.name = name;
		this.value = value;
	}

	public abstract T fromString(String from) throws Exception;

	public abstract String stringValue();

	public T getValue() {
		return value;
	}

	public final void setValue(T value) {
		this.value = value;
		onValueSet(value);
	}

	public void onValueSet(T value) {
	}

	public String getName() {
		return name;
	}
}
