package com.piegoesmoo.toaster2.irc;

import com.piegoesmoo.toaster2.util.MinecraftChatOutput;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ChatComponentText;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/2/2014
 */
public class IRCOutput extends MinecraftChatOutput {
	private static final Minecraft mc = Minecraft.getMinecraft();

	@Override public void log(String s) {
		if (mc.thePlayer != null && mc.theWorld != null) {
			mc.thePlayer.addChatMessage(new ChatComponentText("\247c[IRC]: \247r" + s));
		}
	}

	@Override public void logf(String s, Object... args) {
		log(String.format(s, args));
	}
}
