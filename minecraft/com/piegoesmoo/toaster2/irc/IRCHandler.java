package com.piegoesmoo.toaster2.irc;

import com.piegoesmoo.toaster2.util.TFile;
import net.minecraft.util.EnumChatFormatting;
import org.pircbotx.*;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.JoinEvent;
import org.pircbotx.hooks.managers.ThreadedListenerManager;
import org.pircbotx.hooks.types.GenericMessageEvent;

import java.io.*;
import java.util.Base64;

public class IRCHandler extends ListenerAdapter<PircBotX> {
	private static final IRCOutput LOGGER = new IRCOutput();
	private static final File IRC_FILE = new TFile("irc");

	private final ThreadedListenerManager<PircBotX> listenerManager = new ThreadedListenerManager<>();
	private PircBotX bot;
	private Channel channel;

	private final String username, password;

	public IRCHandler(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public void init() {
		try {
			Configuration<PircBotX> configuration = new Configuration.Builder<>()
					.setListenerManager(listenerManager)
					.setAutoNickChange(true).setName(username)
					.setLogin(username).setNickservPassword(password)
					.setVersion("t2aster - best client")
					.addAutoJoinChannel("#t2aster")
					.setAutoNickChange(true)
					.setServer("irc.freenode.net", 6666).buildConfiguration();
			System.out.println(password);
			listenerManager.addListener(this);
			bot = new PircBotX(configuration);
			bot.startBot();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendMessage(String message) {
		if (channel != null) {
			channel.send().message(message);
			LOGGER.log(userNick(bot.getUserBot()) + ": " + message);
		}
	}

	@Override public void onGenericMessage(GenericMessageEvent<PircBotX> event)
			throws
			Exception {
		LOGGER.log(userNick(event.getUser()) + ": " + event.getMessage());
	}

	@Override public void onJoin(JoinEvent<PircBotX> event) throws
			Exception {
		this.channel = event.getChannel();
	}

	private String userNick(User user) {
		String nick = user.getNick();
		assert channel != null;
		if (user.getUserLevels(channel).contains(UserLevel.OP))
			nick = EnumChatFormatting.LIGHT_PURPLE + nick;
		else
			nick = EnumChatFormatting.GRAY + nick;

		return nick + EnumChatFormatting.RESET;
	}

	public static void saveInfo(String password) {
		try (BufferedWriter writer = new BufferedWriter(
				new FileWriter(IRC_FILE))) {
			writer.write(new String(Base64.getEncoder().encode(password.getBytes())));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String loadInfo() {
		if (IRC_FILE.exists() && !IRC_FILE.isDirectory()) {
			try (BufferedReader reader = new BufferedReader(new FileReader(
					IRC_FILE))) {
				return new String(Base64.getDecoder().decode(reader.readLine().getBytes()));
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		} else {
			try {
				IRC_FILE.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}
}