package com.piegoesmoo.toaster2.profile;

import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.cmd.TCommand;
import com.piegoesmoo.toaster2.util.TFile;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/4/2014
 */
public class ProfileManager {
	private Map<String, Profile> profiles = new HashMap<>();
	private static final File PROFILES_FILE = new TFile("profiles");

	public void init() {
		try {
			if (!PROFILES_FILE.exists())
				PROFILES_FILE.createNewFile();
			else {
				try (ObjectInputStream stream = new ObjectInputStream(new FileInputStream(PROFILES_FILE))) {
					profiles = (Map<String, Profile>) stream.readObject();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		Toaster.getToaster().commandManager.addCommand(new TCommand("profile") {
			@Override public void runCommand(String... args) {
				if (args.length == 1) {
					if (profiles.containsKey(args[1])) {
						profiles.remove(args[0]);
						Toaster.CHAT_OUTPUT.logf("Cleared profile of %s (neutral).", args[0]);
					} else {
						Toaster.CHAT_OUTPUT.errorf("The profile of %s doesn't exist!", args[0]);
					}
				} else if (args.length == 2) {
					Profile profile = getProfile(args[0]);
					try {
						profile = Profile.valueOf(args[1].toUpperCase());
					} catch (Exception e) {
					}
					if (profile != null) {
						profiles.put(args[0], profile);
						Toaster.CHAT_OUTPUT.logf("Set profile of %s to %s.", args[0], profile.toString().toLowerCase());
					} else {
						Toaster.CHAT_OUTPUT.errorf("Invalid profile type: ", args[1]);
					}
				} else {
					Toaster.CHAT_OUTPUT.error("profile <name>");
					Toaster.CHAT_OUTPUT.error("profile <name> <enemy/friend>");
				}
			}
		});
	}

	public void saveProfiles() {
		try (ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(PROFILES_FILE))) {
			stream.writeObject(profiles);
		} catch (Exception e) {
		}
	}

	public Profile getProfile(String name) {
		for (Map.Entry<String, Profile> entry : profiles.entrySet()) {
			if (entry.getKey().equalsIgnoreCase(name)) {
				return entry.getValue();
			}
		}
		return null;
	}

	public boolean isFriend(String name) {
		for (Map.Entry<String, Profile> entry : profiles.entrySet()) {
			if (entry.getKey().equalsIgnoreCase(name))
				return entry.getValue().equals(Profile.FRIEND);
		}
		return false;
	}
}
