package com.piegoesmoo.toaster2.profile;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/4/2014
 */
public enum Profile {
	FRIEND,
	ENEMY
}
