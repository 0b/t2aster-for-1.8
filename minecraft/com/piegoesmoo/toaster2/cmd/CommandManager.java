package com.piegoesmoo.toaster2.cmd;

import com.google.common.reflect.ClassPath;
import com.piegoesmoo.toaster2.Toaster;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/27/2014
 */
public class CommandManager {
	private List<TCommand> commands = new ArrayList<>();

	public void init() {
		try {
			for (ClassPath.ClassInfo classInfo : ClassPath.from(this.getClass().getClassLoader()).getTopLevelClassesRecursive("com.piegoesmoo.toaster2.cmd.cmds")) {
				Class<?> clazz = classInfo.load();
				if (TCommand.class.isAssignableFrom(clazz)) {
					commands.add((TCommand) clazz.newInstance());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		commands.add(new TCommand("help", "cmds") {
			@Override
			public void runCommand(String... args) {
				String s = "Commands (" + commands.size() + "): ";
				for (TCommand p : commands) {
					s += p.aliases[0] + ", ";
				}

				s = s.substring(0, s.length() - 2);
				Toaster.CHAT_OUTPUT.log(s);
			}
		});
	}

	public List<TCommand> getCommands() {
		return commands;
	}

	public boolean runCommand(String s) {
		List<String> args = new ArrayList<>();
		boolean b = s.charAt(0) == '.';
		if (b) {
			Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(s.substring(1));
			while (m.find())
				args.add(m.group(1).replaceAll("\"", ""));
			String cmdName = args.get(0);
			for (TCommand command : commands) {
				for (String s1 : command.aliases) {
					if (s1.equalsIgnoreCase(cmdName)) {
						String[] kek = args.toArray(new String[args.size()]);
						command.runCommand(Arrays.copyOfRange(kek, 1, kek.length));
						return true;
					}
				}
			}

			Toaster.CHAT_OUTPUT.errorf("Invalid command: %s", cmdName);
		}
		return b;
	}

	public void addCommand(TCommand command) {
		commands.add(command);
	}
}
