package com.piegoesmoo.toaster2.cmd;

import net.minecraft.client.Minecraft;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/27/2014
 */
public abstract class TCommand {
	protected static final Minecraft mc = Minecraft.getMinecraft();

	public final String[] aliases;

	public TCommand(String... aliases) {
		this.aliases = aliases;
	}

	public abstract void runCommand(String... args);
}
