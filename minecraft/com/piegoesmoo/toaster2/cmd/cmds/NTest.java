package com.piegoesmoo.toaster2.cmd.cmds;

import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.cmd.TCommand;
import com.piegoesmoo.toaster2.notification.Notification;
import org.apache.commons.lang3.StringEscapeUtils;

import java.util.Random;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/2/2014
 */
public class NTest extends TCommand {
	private final Random random = new Random();

	public NTest() {
		super("\u006e\u0074\u0065\u0073\u0074");
	}

	@Override public void runCommand(String... args) {
		Toaster.getToaster().notificationManager.sendNotification(new Notification("\u0074\u006f\u0070\u0020\u006b\u0065\u006b", StringEscapeUtils.escapeJava("kill self"), StringEscapeUtils.escapeJava("kill self")));
	}
}
