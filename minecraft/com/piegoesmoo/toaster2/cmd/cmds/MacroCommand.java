package com.piegoesmoo.toaster2.cmd.cmds;

import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.cmd.TCommand;
import com.piegoesmoo.toaster2.macro.Macro;
import org.lwjgl.input.Keyboard;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/13/2014
 */
public class MacroCommand extends TCommand {
	public MacroCommand() {
		super("macro");
	}

	@Override public void runCommand(String... args) {
		if (args.length > 1) {
			int key = Keyboard.getKeyIndex(args[0].toUpperCase());
			if (key != Keyboard.KEY_NONE) {
				String result = "";
				for (int i = 1; i < args.length; i++) {
					result += args[i] + " ";
				}
				Toaster.getToaster().macroManager.addMacro(new Macro(result.trim(), key));
				Toaster.CHAT_OUTPUT.log("Macro set.");
			} else {
				Toaster.CHAT_OUTPUT.errorf("Invalid key: %s", args[0]);
			}
		} else if (args.length == 1) {
			int key = Keyboard.getKeyIndex(args[0].toUpperCase());
			if (key != Keyboard.KEY_NONE) {
				Toaster.getToaster().macroManager.removeMacro(key);
				Toaster.CHAT_OUTPUT.log("Macro removed.");
			} else {
				Toaster.CHAT_OUTPUT.errorf("Invalid key: %s", args[0]);
			}
		} else {
			Toaster.CHAT_OUTPUT.error("macro <key> (clears macro)");
			Toaster.CHAT_OUTPUT.error("macro <key> <command>");
		}
	}
}
