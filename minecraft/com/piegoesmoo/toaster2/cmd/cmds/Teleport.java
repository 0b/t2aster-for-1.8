package com.piegoesmoo.toaster2.cmd.cmds;

import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.cmd.TCommand;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/5/2014
 */
public class Teleport extends TCommand {
	public Teleport() {
		super("tele", "tp");
	}

	@Override public void runCommand(String... args) {
		if(args.length == 1) {
			try {
				int dist = Integer.parseInt(args[0]);
				mc.thePlayer.setPosition(mc.thePlayer.posX, mc.thePlayer.posY + dist, mc.thePlayer.posZ);
			} catch (Exception e) {
				Toaster.CHAT_OUTPUT.error("tp <distance>");
			}
		} else {
			Toaster.CHAT_OUTPUT.error("tp <distance>");
		}
	}
}
