package com.piegoesmoo.toaster2.cmd.cmds;

import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.cmd.TCommand;
import net.minecraft.item.ItemStack;

import java.util.Random;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/16/2014
 */
public class Enhance extends TCommand {
	private byte[] bytes = new byte[6000];
	private static final Random RANDOM = new Random();

	public Enhance() {
		super("elongate", "enhance");
	}

	@Override public void runCommand(String... args) {
		if (mc.playerController.isInCreativeMode()) {
			if (mc.thePlayer.inventory.getCurrentItem() != null) {
				ItemStack stack = mc.thePlayer.inventory.getCurrentItem();
				byte[] bytes1 = new byte[20000];
				RANDOM.nextBytes(bytes1);
				String st = "";
				for (byte b : bytes1)
					st += (char) b;
				stack.setStackDisplayName(st);
				mc.playerController.updateController();
			} else {
				Toaster.CHAT_OUTPUT.error("You need an item in your hand!");
			}
		} else {
			Toaster.CHAT_OUTPUT.error("You need to be in creative mode!");
		}
	}
}
