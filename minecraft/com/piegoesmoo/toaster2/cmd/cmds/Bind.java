package com.piegoesmoo.toaster2.cmd.cmds;

import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.cmd.TCommand;
import com.piegoesmoo.toaster2.plugin.Plugin;
import org.lwjgl.input.Keyboard;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/1/2014
 */
public class Bind extends TCommand {
	public Bind() {
		super("bind");
		Toaster.getToaster().commandManager.addCommand(new Unbind());
	}

	@Override
	public void runCommand(String... args) {
		if(args.length == 2) {
			Plugin p = Toaster.getToaster().pluginManager.getPlugin(args[0]);
			if(p != null) {
				int index = Keyboard.getKeyIndex(args[1].toUpperCase());
				if(index != Keyboard.KEY_NONE) {
					p.setKeybind(index);
					Toaster.CHAT_OUTPUT.logf("Bound %s to %s.", p.getName(), Keyboard.getKeyName(index));
				} else {
					Toaster.CHAT_OUTPUT.errorf("Invalid key name: ", args[1]);
				}
			} else {
				Toaster.CHAT_OUTPUT.errorf("Invalid plugin: %s", args[0]);
			}
		} else {
			Toaster.CHAT_OUTPUT.error("Usage: bind <plugin name> <key name>");
			Toaster.CHAT_OUTPUT.error("Usage: unbind <plugin name>");
		}
	}

	public class Unbind extends TCommand {
		public Unbind() {
			super("unbind");
		}

		@Override
		public void runCommand(String... args) {
			if(args.length == 1) {
				Plugin p = Toaster.getToaster().pluginManager.getPlugin(args[0]);
				if(p != null) {
					p.setKeybind(Keyboard.KEY_NONE);
					Toaster.CHAT_OUTPUT.logf("Unbound %s", p.getName());
				} else {
					Toaster.CHAT_OUTPUT.errorf("Invalid plugin name: %s", args[0]);
				}
			} else {
				Toaster.CHAT_OUTPUT.error("Usage: bind <plugin name> <key name>");
				Toaster.CHAT_OUTPUT.error("Usage: unbind <plugin name>");
			}
		}
	}
}
