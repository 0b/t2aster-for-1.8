package com.piegoesmoo.toaster2.cmd.cmds;

import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.cmd.TCommand;
import com.piegoesmoo.toaster2.plugin.Plugin;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/27/2014
 */
public class Toggle extends TCommand {
	public Toggle() {
		super("toggle", "t");
	}

	@Override
	public void runCommand(String... args) {
		if (args.length == 1) {
			for (Plugin p : Toaster.getToaster().pluginManager.getPluginList()) {
				if (p.getName().replaceAll(" ", "").equalsIgnoreCase(args[0])) {
					p.toggle();
					Toaster.CHAT_OUTPUT.log("Toggled " + p.getName() + ".");
					return;
				}
			}
			Toaster.CHAT_OUTPUT.errorf("Invalid plugin: %s.", args[0]);
		} else {
			Toaster.CHAT_OUTPUT.error("t <plugin name>");
		}
	}
}
