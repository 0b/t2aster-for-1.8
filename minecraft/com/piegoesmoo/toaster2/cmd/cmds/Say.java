package com.piegoesmoo.toaster2.cmd.cmds;

import com.piegoesmoo.toaster2.cmd.TCommand;
import net.minecraft.network.play.client.C01PacketChatMessage;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/27/2014
 */
public class Say extends TCommand {
	public Say() {
		super("say");
	}

	@Override
	public void runCommand(String... args) {
		String output = "";
		for (String str : args)
			output = output + str;

		mc.thePlayer.sendQueue.addToSendQueue(new C01PacketChatMessage(output));
	}
}
