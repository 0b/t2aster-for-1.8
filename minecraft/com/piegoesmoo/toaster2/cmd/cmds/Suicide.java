package com.piegoesmoo.toaster2.cmd.cmds;

import com.piegoesmoo.toaster2.cmd.TCommand;
import net.minecraft.network.play.client.C03PacketPlayer;

/**
 * Project: Client
 *
 * @author Justin
 * @since 11/23/2014
 */
public class Suicide extends TCommand {
	public Suicide() {
		super("suicide");
	}

	@Override public void runCommand(String... args) {
		mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY + 0.1, mc.thePlayer.posZ, true));
		mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY - 1337, mc.thePlayer.posZ, true));
	}
}
