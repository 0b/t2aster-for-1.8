package com.piegoesmoo.toaster2.cmd.cmds;

import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.cmd.TCommand;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.property.Property;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/4/2014
 */
public class SetValue extends TCommand {
	public SetValue() {
		super("set", "value");
	}

	@Override public void runCommand(String... args) {
		if (args.length >= 2) {
			if (!args[0].endsWith("keybind")
					&& !args[0].endsWith("state")) {
				Property property = getProperty(args[0].toLowerCase());
				if (property != null) {
					try {
						property.fromString(args[1]);
						Toaster.CHAT_OUTPUT.logf("Property %s has been set to \"%s\".", property.getName(), property.stringValue());
					} catch (Exception e) {
						Toaster.CHAT_OUTPUT.errorf("String \"%s\" for value %s is invalid.", args[1], property.getName());
					}
				} else {
					Toaster.CHAT_OUTPUT.errorf("The value %s does not exist.", args[0].toLowerCase());
				}
			} else {
				Toaster.CHAT_OUTPUT.error("Please use bind or toggle to set the keybind or state (respectively).");
			}
		} else if (args.length == 1) {
			Property property = getProperty(args[0].toLowerCase());
			if (property != null)
				Toaster.CHAT_OUTPUT.logf("Property \"%s\" is set to \"%s\".", property.getName(), property.stringValue());
			else
				Toaster.CHAT_OUTPUT.errorf("Invalid property: \"%s\".", args[0]);
		} else {
			Toaster.CHAT_OUTPUT.error("set <property name> (displays current value)");
			Toaster.CHAT_OUTPUT.error("set <property name> <value>");
			String result = "Properties: ";
			for (Plugin p : Toaster.getToaster().pluginManager.getPluginList())
				for (Property property : p.getPropertyList())
					if (!property.getName().endsWith("keybind")
							&& !property.getName().endsWith("state"))
						result += property.getName() + " ";
			for (Property property : Toaster.getToaster().globalProps)
				result += property.getName() + " ";
			Toaster.CHAT_OUTPUT.log(result.trim());
		}
	}

	public Property<?> getProperty(String name) {
		for (Plugin p : Toaster.getToaster().pluginManager.getPluginList())
			for (Property property : p.getPropertyList())
				if (property.getName().equalsIgnoreCase(name))
					return property;

		for (Property property : Toaster.getToaster().globalProps)
			if (property.getName().equalsIgnoreCase(name))
				return property;

		return null;
	}
}
