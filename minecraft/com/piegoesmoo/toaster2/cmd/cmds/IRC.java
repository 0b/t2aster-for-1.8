package com.piegoesmoo.toaster2.cmd.cmds;

import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.cmd.TCommand;
import com.piegoesmoo.toaster2.irc.IRCHandler;

public class IRC extends TCommand {

	public IRC() {
		super("irc");
	}

	@Override public void runCommand(String... args) {
		if (args.length == 2) {
			if (args[0].equalsIgnoreCase("register")) {
				IRCHandler.saveInfo(args[1]);
				Toaster.CHAT_OUTPUT.log("Password saved! You can now use IRC!");
			}
		}
	}

}
