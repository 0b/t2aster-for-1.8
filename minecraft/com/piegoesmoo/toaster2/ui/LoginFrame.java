package com.piegoesmoo.toaster2.ui;

import javax.swing.*;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/19/2014
 */
public class LoginFrame extends JFrame {
	private final JPanel panel;

	public LoginFrame() {
		super("Login to Minecraft");
		this.add((panel = new JPanel()));
		JTextField field = new JTextField();
		field.setText("Username");
		JPasswordField passwordField = new JPasswordField("Password");
		JButton button = new JButton("Login");
		panel.add(field);
		panel.add(passwordField);
		panel.add(button);

		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
