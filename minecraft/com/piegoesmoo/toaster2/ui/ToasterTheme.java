package com.piegoesmoo.toaster2.ui;

import com.piegoesmoo.toaster2.ui.component.toaster.*;
import net.minecraft.client.gui.FontRenderer;
import org.darkstorm.minecraft.gui.font.UnicodeFontRenderer;
import org.darkstorm.minecraft.gui.theme.AbstractTheme;

import java.awt.*;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/18/2014
 */
public class ToasterTheme extends AbstractTheme {
	private FontRenderer fontRenderer;

	public ToasterTheme() {
		try {
			fontRenderer = new UnicodeFontRenderer(Font.createFont(Font.PLAIN, FontRenderer.class.getResourceAsStream("/assets/minecraft/toaster/Lato-Bold.ttf")).deriveFont(15.0F));
		} catch (Exception e) {
			e.printStackTrace();
			fontRenderer = new UnicodeFontRenderer(new Font("Lato", Font.BOLD, 14));
		}

		installUI(new TButton(this));
		installUI(new TFrame(this));
		installUI(new TPanel());
		installUI(new TCheckBox(this));
		installUI(new TLabel(this));
		installUI(new TSlider(this));
	}

	public FontRenderer getFontRenderer() {
		return fontRenderer;
	}
}
