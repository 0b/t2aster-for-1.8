/*
 * Copyright (c) 2013, DarkStorm (darkstorm@evilminecraft.net)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.piegoesmoo.toaster2.ui;

import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.plugin.Plugin;
import com.piegoesmoo.toaster2.plugin.PluginCategory;
import com.piegoesmoo.toaster2.plugin.internal.KillAura;
import com.piegoesmoo.toaster2.property.properties.BoolProperty;
import net.minecraft.client.Minecraft;
import org.darkstorm.minecraft.gui.AbstractGuiManager;
import org.darkstorm.minecraft.gui.component.BoundedRangeComponent.ValueDisplay;
import org.darkstorm.minecraft.gui.component.Button;
import org.darkstorm.minecraft.gui.component.*;
import org.darkstorm.minecraft.gui.component.Component;
import org.darkstorm.minecraft.gui.component.Frame;
import org.darkstorm.minecraft.gui.component.basic.BasicButton;
import org.darkstorm.minecraft.gui.component.basic.BasicCheckButton;
import org.darkstorm.minecraft.gui.component.basic.BasicFrame;
import org.darkstorm.minecraft.gui.component.basic.BasicSlider;
import org.darkstorm.minecraft.gui.layout.GridLayoutManager;
import org.darkstorm.minecraft.gui.theme.Theme;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Minecraft GUI API
 * <p>
 * This class is not actually intended for use; rather, you should use this as a
 * template for your actual GuiManager, as the creation of frames is highly
 * implementation-specific.
 *
 * @author DarkStorm (darkstorm@evilminecraft.net)
 */
public final class TGuiManager extends AbstractGuiManager {
	private class ModuleFrame extends BasicFrame {
		private ModuleFrame(String title) {
			super(title.toLowerCase());
		}
	}

	private final AtomicBoolean setup;

	public TGuiManager() {
		setup = new AtomicBoolean();
	}

	@Override
	public void setup() {
		if (!setup.compareAndSet(false, true))
			return;
		final Map<PluginCategory, ModuleFrame> categoryFrames = new HashMap<PluginCategory, ModuleFrame>();
		for (Plugin module : Toaster.getToaster().pluginManager.getPluginList()) {
			if (module == Toaster.getToaster().pluginManager.getPlugin(KillAura.class))
				continue;

			ModuleFrame frame = categoryFrames.get(module.getCategory());
			if (frame == null) {
				frame = new ModuleFrame(module.getCategory().name());
				frame.setTheme(new ToasterTheme());
				frame.setLayoutManager(new GridLayoutManager(1, 0));
				frame.setVisible(true);
				frame.setClosable(false);
				frame.setMinimized(true);
				addFrame(frame);
				categoryFrames.put(module.getCategory(), frame);
			}
			//frame.add(new BasicLabel(module.getName()));
			final Plugin updateModule = module;
			CheckButton button = new BasicCheckButton(module.getName().toLowerCase()) {
				@Override public void update() {
					this.setSelected(updateModule.getState());
				}
			};
			button.addButtonListener(button1 -> updateModule.toggle());
			frame.add(button);
		}

		// KILL AURA FRAME
		Frame auraFrame = new ModuleFrame("kill aura");
		Slider speed = new BasicSlider("kill aura speed", Toaster.getToaster().pluginManager.getPlugin(KillAura.class).speed.getValue().doubleValue(), 2, 20);
		speed.addSliderListener(slider1 -> Toaster.getToaster().pluginManager.getPlugin(KillAura.class).speed.setValue(slider1.getValue()));
		speed.setIncrement(0.1);
		speed.setValueDisplay(ValueDisplay.DECIMAL);

		Slider range = new BasicSlider("kill aura range", Toaster.getToaster().pluginManager.getPlugin(KillAura.class).range.getValue().doubleValue(), 2, 6);
		range.addSliderListener(slider1 -> Toaster.getToaster().pluginManager.getPlugin(KillAura.class).range.setValue(slider1.getValue()));
		range.setIncrement(0.1);
		range.setValueDisplay(ValueDisplay.DECIMAL);

		final Plugin killAura = Toaster.getToaster().pluginManager.getPlugin(KillAura.class);

		CheckButton button = new BasicCheckButton(killAura.getName().toLowerCase()) {
			@Override public void update() {
				this.setSelected(killAura.getState());
			}
		};
		button.addButtonListener(button4 -> killAura.toggle());

		auraFrame.setTheme(new ToasterTheme());
		auraFrame.add(button);
		killAura.getPropertyList().stream().filter(property -> property instanceof BoolProperty && (!property.getName().equals("state"))).forEach(property -> {
			BoolProperty boolProp = ((BoolProperty) property);
			CheckButton button1 = new BasicCheckButton(property.getName().substring(9)) {
				@Override public void update() {
					this.setSelected(boolProp.getValue());
				}
			};
			button1.addButtonListener(button2 -> boolProp.setValue(!boolProp.getValue()));
			auraFrame.add(button1);
		});
		auraFrame.add(speed);
		auraFrame.add(range);
		addFrame(auraFrame);
		auraFrame.setLayoutManager(new GridLayoutManager(1, 0));
		auraFrame.setClosable(false);
		auraFrame.setMinimized(true);
		//auraFrame.layoutChildren();

		// Optional equal sizing and auto-positioning
		//resizeComponents();
		Minecraft minecraft = Minecraft.getMinecraft();
		Dimension maxSize = recalculateSizes();
		int offsetX = 5, offsetY = 5;
		int scale = minecraft.gameSettings.guiScale;
		if (scale == 0)
			scale = 1000;
		int scaleFactor = 0;
		while (scaleFactor < scale && minecraft.displayWidth / (scaleFactor + 1) >= 320 && minecraft.displayHeight / (scaleFactor + 1) >= 240)
			scaleFactor++;
		for (Frame frame : getFrames()) {
			frame.setX(offsetX);
			frame.setY(offsetY);
			offsetX += maxSize.width + 5;
			if (offsetX + maxSize.width + 5 > minecraft.displayWidth / scaleFactor) {
				offsetX = 5;
				offsetY += maxSize.height + 5;
			}
		}
	}

	@Override
	protected void resizeComponents() {
		Theme theme = getTheme();
		Frame[] frames = getFrames();
		Button enable = new BasicButton("Enable");
		Button disable = new BasicButton("Disable");
		Dimension enableSize = theme.getUIForComponent(enable).getDefaultSize(enable);
		Dimension disableSize = theme.getUIForComponent(disable).getDefaultSize(disable);
		int buttonWidth = Math.max(enableSize.width, disableSize.width);
		int buttonHeight = Math.max(enableSize.height, disableSize.height);
		for (Frame frame : frames) {
			if (frame instanceof ModuleFrame) {
				for (Component component : frame.getChildren()) {
					if (component instanceof Button) {
						component.setWidth(buttonWidth);
						component.setHeight(buttonHeight);
					}
				}
			}
		}
		recalculateSizes();
	}

	private Dimension recalculateSizes() {
		Frame[] frames = getFrames();
		int maxWidth = 0, maxHeight = 0;
		for (Frame frame : frames) {
			Dimension defaultDimension = frame.getTheme().getUIForComponent(frame).getDefaultSize(frame);
			maxWidth = Math.max(maxWidth, defaultDimension.width);
			frame.setHeight(defaultDimension.height);
			if (frame.isMinimized()) {
				for (Rectangle area : frame.getTheme().getUIForComponent(frame).getInteractableRegions(frame))
					maxHeight = Math.max(maxHeight, area.height);
			} else
				maxHeight = Math.max(maxHeight, defaultDimension.height);
		}
		for (Frame frame : frames) {
			frame.setWidth(maxWidth);
			frame.layoutChildren();
		}
		return new Dimension(maxWidth, maxHeight);
	}
}
