package com.piegoesmoo.toaster2.ui.component.toaster;

import com.piegoesmoo.toaster2.ui.ToasterTheme;
import net.minecraft.client.gui.FontRenderer;
import org.darkstorm.minecraft.gui.component.Button;
import org.darkstorm.minecraft.gui.theme.AbstractComponentUI;
import org.darkstorm.minecraft.gui.util.RenderUtil;

import java.awt.*;

import static org.lwjgl.opengl.GL11.*;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/18/2014
 */
public class TButton extends AbstractComponentUI<Button> {
	private final ToasterTheme theme;
	private final FontRenderer fr;

	public TButton(ToasterTheme theme) {
		super(Button.class);

		this.theme = theme;
		this.fr = theme.getFontRenderer();
		background = new Color(0x2A4445);
	}

	@Override protected void renderComponent(Button component) {
		translateComponent(component, false);
		Rectangle area = component.getArea();
		glEnable(GL_BLEND);
		glDisable(GL_CULL_FACE);
		glDisable(GL_TEXTURE_2D);

		RenderUtil.setColor(background);
		glBegin(GL_QUADS);
		{
			glVertex2d(0, 0);
			glVertex2d(0, area.height);
			glVertex2d(area.width, area.height);
			glVertex2d(area.width, 0);
		}
		glEnd();

		glEnable(GL_TEXTURE_2D);
		fr.drawStringWithShadow(component.getText(),
				(area.width / 2) - (fr.getStringWidth(component.getText()) / 2),
				(area.height / 2) - (fr.FONT_HEIGHT / 2), 0xFFFFFF);
		glEnable(GL_CULL_FACE);
		glDisable(GL_BLEND);
		translateComponent(component, true);
	}

	@Override protected Dimension getDefaultComponentSize(Button component) {
		return new Dimension(50, 20);
	}
}
