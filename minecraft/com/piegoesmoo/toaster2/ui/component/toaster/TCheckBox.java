package com.piegoesmoo.toaster2.ui.component.toaster;

import com.piegoesmoo.toaster2.ui.ToasterTheme;
import org.darkstorm.minecraft.gui.component.CheckButton;
import org.darkstorm.minecraft.gui.theme.AbstractComponentUI;
import org.darkstorm.minecraft.gui.util.RenderUtil;

import java.awt.*;

import static org.lwjgl.opengl.GL11.*;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/18/2014
 */
public class TCheckBox extends AbstractComponentUI<CheckButton> {
	private final ToasterTheme theme;

	public TCheckBox(ToasterTheme theme) {
		super(CheckButton.class);

		this.theme = theme;
		background = new Color(0x1E90FF);
		foreground = new Color(0x565656);
	}

	@Override protected void renderComponent(CheckButton component) {
		translateComponent(component, false);
		Rectangle area = component.getArea();
		Point mouse = RenderUtil.calculateMouseLocation();
		glEnable(GL_BLEND);
		glDisable(GL_CULL_FACE);
		glDisable(GL_TEXTURE_2D);
		Color panelBG = component.getParent().getBackgroundColor();
		mouse.x -= component.getParent().getX();
		mouse.y -= component.getParent().getY();
		Color c = component.isSelected() ? background : area.contains(mouse) ? panelBG.brighter().brighter() : panelBG.brighter();
		RenderUtil.setColor(c);
		glBegin(GL_QUADS);
		{
			glVertex2d(0, 0);
			glVertex2d(area.width, 0);
			glVertex2d(area.width, area.height);
			glVertex2d(0, area.height);
		}
		glEnd();


		glEnable(GL_TEXTURE_2D);
		theme.getFontRenderer().drawStringWithShadow(component.getText(),
				area.width - theme.getFontRenderer().getStringWidth(component.getText()) - 2,
				area.height / 2 - theme.getFontRenderer().FONT_HEIGHT / 2, component.isSelected() ? 0xFFFFFF : 0xCCCCCC);
		glEnable(GL_CULL_FACE);
		glDisable(GL_BLEND);
		translateComponent(component, true);
	}

	@Override
	protected Dimension getDefaultComponentSize(CheckButton component) {
		return new Dimension(100, theme.getFontRenderer().FONT_HEIGHT + 4);
	}

	@Override
	protected Rectangle[] getInteractableComponentRegions(CheckButton component) {
		return new Rectangle[]{new Rectangle(0, 0, component.getWidth(),
				component.getHeight())};
	}

	@Override
	protected void handleComponentInteraction(CheckButton component,
	                                          Point location, int button) {
		if (location.x <= component.getWidth()
				&& location.y <= component.getHeight() && button == 0)
			component.press();
	}
}
