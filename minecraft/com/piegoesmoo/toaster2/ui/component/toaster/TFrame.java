package com.piegoesmoo.toaster2.ui.component.toaster;

import com.piegoesmoo.toaster2.Toaster;
import com.piegoesmoo.toaster2.events.EventIngameRender;
import com.piegoesmoo.toaster2.ui.ToasterTheme;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.ResourceLocation;
import org.darkstorm.minecraft.gui.component.Frame;
import org.darkstorm.minecraft.gui.layout.Constraint;
import org.darkstorm.minecraft.gui.theme.AbstractComponentUI;
import org.darkstorm.minecraft.gui.util.RenderUtil;

import java.awt.*;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/18/2014
 */
public class TFrame extends AbstractComponentUI<Frame> {
	//TODO RENDER THESE
	private static final ResourceLocation OPEN = new ResourceLocation("toaster/icon/panel/open.png");
	private static final ResourceLocation CLOSED = new ResourceLocation("toaster/icon/panel/closed.png");
	private static final ResourceLocation PIN = new ResourceLocation("toaster/icon/panel/pin.png");

	private final FontRenderer fr;
	private final ToasterTheme theme;
	private final Map<String, ResourceLocation> icons = new HashMap<>();

	public TFrame(ToasterTheme toasterTheme) {
		super(Frame.class);

		fr = toasterTheme.getFontRenderer();
		background = new Color(0x1A1A1A);
		foreground = new Color(0x1E90FF);
		this.theme = toasterTheme;
	}

	@Override protected void renderComponent(Frame component) {
		if (!icons.containsKey(component.getTitle())) {
			try {
				if (new File(Toaster.class.getClassLoader().getResource("assets/minecraft/toaster/icon/" + component.getTitle().toUpperCase() + ".png").toURI()).exists())
					icons.put(component.getTitle(), new ResourceLocation("toaster/icon/" + component.getTitle().toUpperCase() + ".png"));
			} catch (Exception e) {

			}
		}

		if (component.getX() <= 5)
			component.setX(0);
		if (component.getY() <= 5)
			component.setY(0);
		if (component.getX() + component.getWidth() >= EventIngameRender.EVENT_INGAME_RENDER.getScaledResolution().getScaledWidth() - 5)
			component.setX(EventIngameRender.EVENT_INGAME_RENDER.getScaledResolution().getScaledWidth() - component.getWidth());
		if (component.getY() + component.getHeight() >= EventIngameRender.EVENT_INGAME_RENDER.getScaledResolution().getScaledHeight() - 5)
			component.setY(EventIngameRender.EVENT_INGAME_RENDER.getScaledResolution().getScaledHeight() - component.getHeight());

		int fontHeight = fr.FONT_HEIGHT;
		translateComponent(component, false);
		Rectangle area = new Rectangle(component.getArea());
		glEnable(GL_BLEND);
		glDisable(GL_CULL_FACE);
		glDisable(GL_TEXTURE_2D);
		if (component.isMinimized())
			area.height = fontHeight + 4;
		RenderUtil.setColor(component.getBackgroundColor());
		glBegin(GL_QUADS);
		{
			glVertex2d(0, 0);
			glVertex2d(area.width, 0);
			glVertex2d(area.width, area.height);
			glVertex2d(0, area.height);
		}
		glEnd();

		glLineWidth(1);
		RenderUtil.setColor(background.brighter());
		glBegin(GL_LINE_LOOP);
		{
			glVertex2d(0, 0);
			glVertex2d(area.width, 0);
			glVertex2d(area.width, area.height);
			glVertex2d(0, area.height);
		}
		glEnd();

		int offset = component.getWidth() - 10;
		Point mouse = RenderUtil.calculateMouseLocation();
		org.darkstorm.minecraft.gui.component.Component parent = component;
		while (parent != null) {
			mouse.x -= parent.getX();
			mouse.y -= parent.getY();
			parent = parent.getParent();
		}
		glScaled(0.5, 0.5, 0.5);
		offset *= 2;
		glEnable(GL_TEXTURE_2D);

		glColor4f(1, 1, 1, 1);

		if (component.isPinnable()) {
			RenderUtil.setColor(component.isPinned() ? foreground : background.brighter().brighter());
			RenderUtil.drawTexturedRectangle(PIN, offset, 5, 16, 16, 0, 0, 16, 16, 16, 16);
			offset -= 20;
		}

		if (component.isMinimizable()) {
			RenderUtil.setColor(!component.isMinimized() ? foreground : background.brighter().brighter());
			if (component.isMinimized()) {
				RenderUtil.drawTexturedRectangle(CLOSED, offset, 5, 16, 16, 0, 0, 16, 16, 16, 16);
			} else {
				RenderUtil.drawTexturedRectangle(OPEN, offset, 5, 16, 16, 0, 0, 16, 16, 16, 16);
			}
		}
		glScaled(2, 2, 2);

		glColor4f(0f, 0f, 0f, 1f);
		glLineWidth(1.0f);

		glScaled(0.5, 0.5, 0.5);
		boolean b = icons.containsKey(component.getTitle());
		if (b) {
			glColor4f(1, 1, 1, 1);
			RenderUtil.drawTexturedRectangle(icons.get(component.getTitle()), 4, 6, 16, 16, 0, 0, 16, 16, 16, 16);
		}
		glScaled(2, 2, 2);
		fr.drawStringWithShadow(component.getTitle(), b ? 12 : 2, 2, 0xFFFFFF);
		glEnable(GL_CULL_FACE);
		glDisable(GL_BLEND);
		translateComponent(component, true);
	}

	@Override
	protected Rectangle getContainerChildRenderArea(Frame container) {
		Rectangle area = new Rectangle(container.getArea());
		area.x = 2;
		area.y = theme.getFontRenderer().FONT_HEIGHT + 6;
		area.width -= 4;
		area.height -= theme.getFontRenderer().FONT_HEIGHT + 8;
		return area;
	}

	@Override
	protected Dimension getDefaultComponentSize(Frame component) {
		org.darkstorm.minecraft.gui.component.Component[] children = component.getChildren();
		Rectangle[] areas = new Rectangle[children.length];
		Constraint[][] constraints = new Constraint[children.length][];
		for (int i = 0; i < children.length; i++) {
			org.darkstorm.minecraft.gui.component.Component child = children[i];
			Dimension size = child.getTheme().getUIForComponent(child).getDefaultSize(child);
			areas[i] = new Rectangle(0, 0, size.width, size.height);
			constraints[i] = component.getConstraints(child);
		}
		Dimension size = component.getLayoutManager().getOptimalPositionedSize(
				areas, constraints);
		size.width += 4;
		size.height += theme.getFontRenderer().FONT_HEIGHT + 8;
		return size;
	}

	@Override
	protected Rectangle[] getInteractableComponentRegions(Frame component) {
		return new Rectangle[]{new Rectangle(0, 0, component.getWidth(),
				theme.getFontRenderer().FONT_HEIGHT + 4)};
	}

	@Override
	protected void handleComponentInteraction(Frame component, Point location,
	                                          int button) {
		if (button != 0)
			return;
		int offset = component.getWidth() - 2;
		int textHeight = theme.getFontRenderer().FONT_HEIGHT;
		if (component.isClosable()) {
			if (location.x >= offset - textHeight && location.x <= offset
					&& location.y >= 2 && location.y <= textHeight + 2) {
				component.close();
				return;
			}
			offset -= textHeight + 2;
		}
		if (component.isPinnable()) {
			if (location.x >= offset - textHeight && location.x <= offset
					&& location.y >= 2 && location.y <= textHeight + 2) {
				component.setPinned(!component.isPinned());
				return;
			}
			offset -= textHeight + 2;
		}
		if (component.isMinimizable()) {
			if (location.x >= offset - textHeight && location.x <= offset
					&& location.y >= 2 && location.y <= textHeight + 2) {
				component.setMinimized(!component.isMinimized());
				return;
			}
			offset -= textHeight + 2;
		}
		if (location.x >= 0 && location.x <= offset && location.y >= 0
				&& location.y <= textHeight + 4) {
			component.setDragging(true);
			return;
		}
	}
}
