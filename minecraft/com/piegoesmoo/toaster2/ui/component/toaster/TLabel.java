package com.piegoesmoo.toaster2.ui.component.toaster;

import com.piegoesmoo.toaster2.ui.ToasterTheme;
import org.darkstorm.minecraft.gui.component.Label;
import org.darkstorm.minecraft.gui.theme.AbstractComponentUI;

import java.awt.*;

import static org.lwjgl.opengl.GL11.*;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/18/2014
 */
public class TLabel extends AbstractComponentUI<Label> {
	private final ToasterTheme theme;

	public TLabel(ToasterTheme theme) {
		super(Label.class);
		this.theme = theme;
	}

	@Override protected void renderComponent(Label label) {
		translateComponent(label, false);
		int x = 0, y = 0;
		switch (label.getHorizontalAlignment()) {
			case CENTER:
				x += label.getWidth() / 2
						- theme.getFontRenderer().getStringWidth(label.getText())
						/ 2;
				break;
			case RIGHT:
				x += label.getWidth()
						- theme.getFontRenderer().getStringWidth(label.getText())
						- 2;
				break;
			default:
				x += 2;
		}
		switch (label.getVerticalAlignment()) {
			case TOP:
				y += 2;
				break;
			case BOTTOM:
				y += label.getHeight() - theme.getFontRenderer().FONT_HEIGHT - 2;
				break;
			default:
				y += label.getHeight() / 2 - theme.getFontRenderer().FONT_HEIGHT
						/ 2;
		}
		glEnable(GL_BLEND);
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_CULL_FACE);
		theme.getFontRenderer().drawString(label.getText(), x, y,
				0xFFFFFF);
		glEnable(GL_CULL_FACE);
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
		translateComponent(label, true);
	}

	@Override protected Dimension getDefaultComponentSize(Label component) {
		return new Dimension(theme.getFontRenderer().getStringWidth(
				component.getText()) + 4,
				theme.getFontRenderer().FONT_HEIGHT + 4);
	}
}
