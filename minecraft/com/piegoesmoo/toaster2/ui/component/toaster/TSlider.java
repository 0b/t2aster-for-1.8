package com.piegoesmoo.toaster2.ui.component.toaster;

import com.piegoesmoo.toaster2.ui.ToasterTheme;
import net.minecraft.client.gui.FontRenderer;
import org.darkstorm.minecraft.gui.component.Container;
import org.darkstorm.minecraft.gui.component.Slider;
import org.darkstorm.minecraft.gui.theme.AbstractComponentUI;
import org.darkstorm.minecraft.gui.util.RenderUtil;
import org.lwjgl.input.Mouse;

import java.awt.*;
import java.text.DecimalFormat;

import static org.lwjgl.opengl.GL11.*;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/18/2014
 */
public class TSlider extends AbstractComponentUI<Slider> {
	private final ToasterTheme theme;

	public TSlider(ToasterTheme theme) {
		super(Slider.class);

		this.theme = theme;

		background = new Color(0x1E90FF);
		foreground = background;
	}

	@Override
	protected void renderComponent(Slider component) {
		translateComponent(component, false);
		glEnable(GL_BLEND);
		glDisable(GL_CULL_FACE);
		Rectangle area = component.getArea();
		FontRenderer fontRenderer = theme.getFontRenderer();
		fontRenderer.drawString(component.getText(), 0, 0, 0xFFFFFF);
		String content = null;
		switch (component.getValueDisplay()) {
			case DECIMAL:
				content = String.format("%,.1f", component.getValue());
				break;
			case INTEGER:
				content = String.format("%,d", Math.round(component.getValue()));
				break;
			case PERCENTAGE:
				int percent = (int) Math.round((component.getValue() - component.getMinimumValue()) / (component.getMaximumValue() - component.getMinimumValue()) * 100D);
				content = String.format("%d%%", percent);
			default:
		}
		glDisable(GL_TEXTURE_2D);

		RenderUtil.setColor(component.getParent().getBackgroundColor().brighter());
		glBegin(GL_QUADS);
		{
			glVertex2d(0, 0);
			glVertex2d(area.width, 0);
			glVertex2d(area.width, area.height);
			glVertex2d(0, area.height);
		}
		glEnd();

		double sliderPercentage = (Double.parseDouble(new DecimalFormat("##.#").format(component.getValue())) - component.getMinimumValue()) / (component.getMaximumValue() - component.getMinimumValue());
		RenderUtil.setColor(background);
		glLineWidth(1);
		glBegin(GL_QUADS);
		{
			glVertex2d(0, 0);
			glVertex2d(area.width * sliderPercentage, 0);
			glVertex2d(area.width * sliderPercentage, area.height);
			glVertex2d(0, area.height);
		}
		glEnd();

		RenderUtil.setColor(component.getParent().getBackgroundColor().brighter().brighter());

		glBegin(GL_QUADS);
		{
			glVertex2d((area.width * sliderPercentage) - 1, 0);
			glVertex2d((area.width * sliderPercentage) - 1, area.height);
			glVertex2d((area.width * sliderPercentage) + 1, area.height);
			glVertex2d((area.width * sliderPercentage) + 1, 0);
		}
		glEnd();

		glEnable(GL_TEXTURE_2D);
		fontRenderer.drawString(component.getText(), 2, 2, 0xFFFFFF);
		if (content != null) {
			String suffix = component.getContentSuffix();
			if (suffix != null && !suffix.trim().isEmpty())
				content = content.concat(" ").concat(suffix);
			fontRenderer.drawString(content, component.getWidth() - fontRenderer.getStringWidth(content) - 2, 2, 0xFFFFFF);
		}
		glDisable(GL_BLEND);
		glEnable(GL_CULL_FACE);
		translateComponent(component, true);
	}

	@Override
	protected Dimension getDefaultComponentSize(Slider component) {
		return new Dimension(100, theme.getFontRenderer().FONT_HEIGHT + 4);
	}

	@Override
	protected Rectangle[] getInteractableComponentRegions(Slider component) {
		return new Rectangle[]{new Rectangle(0, 0, component.getWidth(),
				component.getHeight())};
	}

	@Override
	protected void handleComponentInteraction(Slider component, Point location, int button) {
		if (getInteractableComponentRegions(component)[0].contains(location) && button == 0)
			if (Mouse.isButtonDown(button) && !component.isValueChanging())
				component.setValueChanging(true);
			else if (!Mouse.isButtonDown(button) && component.isValueChanging())
				component.setValueChanging(false);
	}

	@Override
	protected void handleComponentUpdate(Slider component) {
		if (component.isValueChanging()) {
			if (!Mouse.isButtonDown(0)) {
				component.setValueChanging(false);
				return;
			}
			Point mouse = RenderUtil.calculateMouseLocation();
			Container parent = component.getParent();
			if (parent != null)
				mouse.translate(-parent.getX(), -parent.getY());
			double percent = (double) mouse.x / (double) component.getWidth();
			double value = component.getMinimumValue() + (percent * (component.getMaximumValue() - component.getMinimumValue()));
			component.setValue(Double.parseDouble(new DecimalFormat("##.#").format(value)));
		}
	}
}
