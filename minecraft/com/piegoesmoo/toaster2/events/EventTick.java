package com.piegoesmoo.toaster2.events;

import com.darkmagician6.eventapi.events.Event;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/20/2014
 */
public class EventTick implements Event {
	public static final EventTick EVENT_TICK = new EventTick();
	private EventTick() {}
}
