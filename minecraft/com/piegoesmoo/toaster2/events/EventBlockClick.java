package com.piegoesmoo.toaster2.events;

import com.darkmagician6.eventapi.events.Event;
import com.piegoesmoo.toaster2.util.EnumEventType;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/17/2014
 */
public class EventBlockClick implements Event {
	private BlockPos blockPos;
	private EnumFacing facing;
	public final EnumEventType type;

	public EventBlockClick(BlockPos blockPos, EnumFacing facing, EnumEventType type) {
		this.blockPos = blockPos;
		this.facing = facing;
		this.type = type;
	}

	public BlockPos getBlockPos() {
		return blockPos;
	}

	public EnumFacing getFacing() {
		return facing;
	}

	public void setBlockPos(BlockPos blockPos) {
		this.blockPos = blockPos;
	}

	public void setFacing(EnumFacing facing) {
		this.facing = facing;
	}
}
