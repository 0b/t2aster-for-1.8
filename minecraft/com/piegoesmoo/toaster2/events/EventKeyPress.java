package com.piegoesmoo.toaster2.events;

import com.darkmagician6.eventapi.events.Event;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/20/2014
 */
public class EventKeyPress implements Event {
	private final int key;

	public EventKeyPress(int key) {
		this.key = key;
	}

	public int getKey() {
		return key;
	}
}
