package com.piegoesmoo.toaster2.events;

import com.darkmagician6.eventapi.events.Event;
import net.minecraft.client.gui.ScaledResolution;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/5/2014
 */
public class EventIngameRender implements Event {
	public static final EventIngameRender EVENT_INGAME_RENDER = new EventIngameRender();
	private EventIngameRender() {}

	private ScaledResolution scaledResolution;

	public ScaledResolution getScaledResolution() {
		return scaledResolution;
	}

	public void setScaledResolution(ScaledResolution scaledResolution) {
		this.scaledResolution = scaledResolution;
	}
}
