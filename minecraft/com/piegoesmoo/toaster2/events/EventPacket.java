package com.piegoesmoo.toaster2.events;

import com.darkmagician6.eventapi.events.Event;
import com.darkmagician6.eventapi.events.callables.EventCancellable;
import net.minecraft.network.Packet;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/11/2014
 */
public class EventPacket extends EventCancellable {
	public final PacketType type;
	private Packet packet;

	public EventPacket(PacketType type, Packet packet) {
		this.type = type;
	}

	public Packet getPacket() {
		return packet;
	}

	public void setPacket(Packet packet) {
		this.packet = packet;
	}

	public enum PacketType {
		SEND,
		RECEIVE
	}
}
