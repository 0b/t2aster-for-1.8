package com.piegoesmoo.toaster2.events;

import com.darkmagician6.eventapi.events.callables.EventCancellable;
import com.piegoesmoo.toaster2.util.EnumEventType;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/27/2014
 */
public class EventUpdate extends EventCancellable {
	public final EnumEventType type;

	public EventUpdate(EnumEventType type) {
		this.type = type;
	}
}
