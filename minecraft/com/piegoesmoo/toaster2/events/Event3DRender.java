package com.piegoesmoo.toaster2.events;

import com.darkmagician6.eventapi.events.Event;
import com.piegoesmoo.toaster2.util.EnumEventType;

import static org.lwjgl.opengl.GL11.*;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/3/2014
 */
public class Event3DRender implements Event {
	private final EnumEventType type;

	private Event3DRender(EnumEventType type) {
		this.type = type;
	}

	public static final Event3DRender RENDER_NOBOB = new Event3DRender(EnumEventType.PRE);
	public static final Event3DRender RENDER = new Event3DRender(EnumEventType.POST);

	public EnumEventType getType() {
		return type;
	}
	public static void enableGL3D(float lineWidth) {
		glDisable(GL_ALPHA_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST);
		glDepthMask(false);
		glEnable(GL_CULL_FACE);
		glEnable(GL_LINE_SMOOTH);
		glHint(GL_LINE_SMOOTH_HINT, GL_FASTEST);
		glDisable(GL_LIGHTING);
	}

	public static void disableGL3D() {
		glEnable(GL_LIGHTING);
		glDisable(GL_LINE_SMOOTH);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);
		glEnable(GL_ALPHA_TEST);
		glDepthMask(true);
		glCullFace(GL_BACK);
	}
}
