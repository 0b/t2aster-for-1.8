package com.piegoesmoo.toaster2.events;

import com.darkmagician6.eventapi.events.Event;

/**
 * Project: Client
 *
 * @author Justin
 * @since 10/13/2014
 */
public class EventMove implements Event {
	private float hMod = 1;
	private float vMod = 1;

	public float gethMod() {
		return hMod;
	}

	public float getvMod() {
		return vMod;
	}

	public void sethMod(float hMod) {
		this.hMod = hMod;
	}

	public void setvMod(float vMod) {
		this.vMod = vMod;
	}
}
