package com.piegoesmoo.toaster2.keybind;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;
import com.piegoesmoo.toaster2.events.EventKeyPress;
import com.piegoesmoo.toaster2.events.EventTick;
import com.piegoesmoo.toaster2.ui.TGuiManager;
import com.piegoesmoo.toaster2.ui.ToasterTheme;
import net.minecraft.client.Minecraft;
import org.darkstorm.minecraft.gui.GuiManager;
import org.darkstorm.minecraft.gui.util.GuiManagerDisplayScreen;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.List;

/**
 * Project: Client
 *
 * @author Justin
 * @since 9/20/2014
 */
public class KeybindManager {
	private GuiManager guiManager;
	private final List<Bindable> bindables = new ArrayList<>();

	public KeybindManager() {
		EventManager.register(this);
	}

	public void init() {
	}

	@EventTarget
	public void onKey(EventKeyPress eventKeyPress) {
		if (eventKeyPress.getKey() == Keyboard.KEY_GRAVE) {
			Minecraft.getMinecraft().displayGuiScreen(new GuiManagerDisplayScreen(guiManager));
		}
		bindables.stream().filter(p -> p.getKeybind() == eventKeyPress.getKey()).forEach(Bindable::fire);
	}

	@EventTarget public void onTick(EventTick eventTick) {
		getGuiManager().update();
	}

	public void addKeybindable(Bindable k) {
		bindables.add(k);
	}

	public List<Bindable> getBindables() {
		return bindables;
	}

	public GuiManager getGuiManager() {
		if (guiManager == null) {
			guiManager = new TGuiManager();
			guiManager.setTheme(new ToasterTheme());
			guiManager.setup();
		}
		return guiManager;
	}
}
